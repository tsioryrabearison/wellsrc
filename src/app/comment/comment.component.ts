import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataServices } from '../data.service';
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  annonce: any;
  users: any;
  user: any;
  lang: string;
  constructor(
    private dialogRef: MatDialogRef<CommentComponent>,
    private dialog: MatDialog,
    private dts: DataServices,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
      this.dts.getLang().subscribe(l => this.lang = l);
      this.annonce = data.annonce;
      this.users = data.users;
      this.dts.user.subscribe(u => this.user = u);
      this.scroll();
  }
  scroll() {
    const scrollmess = setInterval(() => {
      const messageContainer = document.getElementById('comment_content');
      if (messageContainer)  {
        messageContainer.scrollTo({top: messageContainer.scrollHeight});
        clearInterval(scrollmess);
      }
    }, 10);

  }
  ngOnInit() {
  }
  findUser(_id_user) {
      return this.users.find(u => u._id === _id_user);
  }
  addComment(comment) {
    if (comment.value.trim() !== '') {
      const value = {
        _id_user: this.user._id,
        _id_client: this.user._id,
        comment: comment.value.trim()
      };
      comment.value = '';
      this.dts.updateReaction(this.annonce._id, 'comment', value)
          .then( () => {
              this.dts.getAnnonce(this.annonce._id).then(data => {
                  this.annonce = data.body[0];
                  this.scroll();
              });
          });
    }
  }
  close(){
    this.data.callback();
    this. dialogRef.close();
  }
}
