import { Component } from '@angular/core';
import { FeedComponent } from './feed/feed.component';

import { DataServices } from './data.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  constructor(private _dataService: DataServices, private router: Router, private router2: ActivatedRoute ) {
  }
}
