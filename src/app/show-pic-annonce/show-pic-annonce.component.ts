import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataServices } from '../data.service';
import * as $ from 'jquery';
import * as moment from 'moment';
@Component({
  selector: 'app-show-pic-annonce',
  templateUrl: './show-pic-annonce.component.html',
  styleUrls: [ '../feed/feed.component.scss', './show-pic-annonce.component.scss']
})
export class ShowPicAnnonceComponent implements OnInit {
  trano: Object;
  lang: any;
  db: any;
  constructor(
    private dialogRef: MatDialogRef<ShowPicAnnonceComponent>,
    private dts: DataServices,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
      this.dts.getLang().subscribe(l => this.lang = l);
      this.dts.getDbLang().subscribe(db => this.db = db);
      this.trano = data.annonce;
      const intervale = setInterval(() => {
        if ($('.carousel-item')) {
          $('.carousel-control').click(function(e) {
            console.log('prevent');
            e.preventDefault();
          });
          $($('.carousel-item')[0]).addClass('active');
          clearInterval(intervale);
        }
      }, 10);
      console.log(this.trano);
  }

  ngOnInit() {
  }
  closeDialog() {
    this.dialogRef.close();
  }
  getTime(date) {
    return moment(date).locale(this.lang.toLowerCase()).fromNow();
  }

}
