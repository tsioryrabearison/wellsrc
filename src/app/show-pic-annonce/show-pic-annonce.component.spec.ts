import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPicAnnonceComponent } from './show-pic-annonce.component';

describe('ShowPicAnnonceComponent', () => {
  let component: ShowPicAnnonceComponent;
  let fixture: ComponentFixture<ShowPicAnnonceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPicAnnonceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPicAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
