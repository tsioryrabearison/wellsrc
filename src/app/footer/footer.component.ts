import { Component, OnInit } from '@angular/core';
import { DataServices } from '../data.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  db: any;
  lang = 'FR';

  constructor( public ds: DataServices) {
    ds.getLang().subscribe(lang => this.lang = lang)
    ds.getDbLang().subscribe(data => this.db = data)
   }

  ngOnInit() {
  }

}
