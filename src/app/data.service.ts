import { Injectable } from '@angular/core';
// import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Socket } from 'ng-socket-io';


@Injectable()

export class DataServices {

    serverHost = 'https://server.wellcoloc.com/api';
    user: BehaviorSubject<any>;
    _moment: any;
    annonceNotif: BehaviorSubject<number>;
    msgNotif: BehaviorSubject<number>;
    Lang: BehaviorSubject<any>;
    dbLang: BehaviorSubject<any>;

    constructor(public http: HttpClient,
            private socket: Socket
        ) {
        // moment.locale('fr');
        // this._moment = moment;
        this.user = new BehaviorSubject({});
        this.user.asObservable();
        this.annonceNotif = new BehaviorSubject(0);
        this.annonceNotif.asObservable();
        this.msgNotif = new BehaviorSubject(0);
        this.msgNotif.asObservable();
        this.Lang = new BehaviorSubject('EN');
        this.dbLang = new BehaviorSubject(null);
        this.initJson();
        console.log('dts initializing')
        // this.serverHost = 'https://server.wellcoloc.com';
    }
    public getLang(): Observable<string> {
        return this.Lang.asObservable();
      }

      public getDbLang(): Observable<any> {
        return this.dbLang.asObservable()
      }
      public setLang(newValue: string): void {
        this.Lang.next(newValue);
      }

       public initJson(): any {
         console.log('init language db...');
        return this.http
        .get('assets/view.json')
        .toPromise()
        .then(data => {
          this.dbLang.next(data);
          console.log('Language loaded!');
        });
      }

      public getText(text): any {
        return this.initJson()
          .then(data => data.json()[text]);
      }
    login(pseudo, password): Promise<any> {
        return this.http
            .post(`${this.serverHost}/user/login`, { pseudo: pseudo, password: password })
            .toPromise().then(data => data);
    }

    logout(): Promise<any> {
        let _id_user = null;

        this.user.subscribe((u: any) => {
            _id_user = u._id;
        });

        return this.http
            .post(`${this.serverHost}/user/logout`, { _id_user: _id_user})
            .toPromise().then(data => data);
    }

    save(collection, value): Promise<any> {
        return this.http
            .post(`${this.serverHost}/crud/save`, {collection: collection, value: value})
            .toPromise().then(data => data);
    }

    getUrlFileUploaded(folder, fileName, blob): Promise<any> {
        return this.http
            .post(`${this.serverHost}/crud/uploadFile`, {
                folder: folder,
                name: fileName,
                blob: blob })
            .toPromise().then(data => data);
    }

    getUserData(_id_user): Promise<any> {
        return this.http
            .get(`${this.serverHost}/user/${_id_user}`)
            .toPromise().then(data => data);
    }

    getAnnonce(_id_annonce): Promise<any> {
        return this.http
            .get(`${this.serverHost}/crud/get?ObjectId=${_id_annonce}&collection=annonce`)
            .toPromise().then(data => data);
    }

    getUserAnnonces(): Promise<any> {
        let user;
        this.user.subscribe(u => user = u);
        return this.http
            .get(`${this.serverHost}/crud/q?_id_user=${user._id}&collection=annonce`)
            .toPromise().then(data => data);
    }

    getAllUsers(): Promise<any> {
        return this.http
            .get(`${this.serverHost}/crud/get?collection=user`)
            .toPromise().then(data => data);

    }

    getAllAnnonces(): Promise<any> {
        return this.http
            .get(`${this.serverHost}/crud/get?collection=annonce`)
            .toPromise().then(data => data);
    }

    getNotificationData(): Promise<any> {
        let _id_user;
        this.user.subscribe(u => { _id_user = u._id;
        });

        return this.http
            .get(`${this.serverHost}/notification/${_id_user}`)
            .toPromise().then(data => data);
    }

    getNotificationCounter(type): Promise<any> {
        let _id_user;
        this.user.subscribe(u => {
            _id_user = u._id;
        });

        return this.http
            .get(`${this.serverHost}/notification/${_id_user}/counter/?type=${type}`)
            .toPromise().then(data => data);
    }

    getAllChat(): Promise<any> {

        let _id_user;
        this.user.subscribe(u => {
            _id_user = u._id;
        });
        // console.log(_id_user);
        return this.http
            .get(`${this.serverHost}/message/${_id_user}/chat`)
            .toPromise().then(data => data);
    }

    getChat(_id_chat?, _id_client?): Promise<any> {
        let q: any;
        let _id_user = null;
        this.user.subscribe(u => {
            _id_user = u._id;
        });

        if (_id_client) {
            q = `_id_client=${_id_client}&_id_user=${ _id_user }`;
        }

        if (_id_chat) {
            q = `_id_chat=${_id_chat}`;
        }

        return this.http
            .get(`${this.serverHost}/message/chat?${q}`)
            .toPromise().then(data => data);
    }

    deleteMessage(_id): Promise<any> {
        return this.http
            .post(`${this.serverHost}/message/delete`, {
                _id_chat: _id,
            }).toPromise().then(data => data);
    }

    deleteNotif(type, _id_client?): Promise<any> {
        let _id_user;
        this.user.subscribe(u => _id_user = u._id);

        return this.http
            .post(`${this.serverHost}/notification/delete`, {
                _id: _id_user,
                _id_client: _id_client,
                type: type
            }).toPromise().then(data => data);
    }

    deleteAnnonce(_id): Promise<any> {
        return this.http
            .post(`${this.serverHost}/crud/delete`, {
                collection: 'annonce',
                _id: _id
            })
            .toPromise().then(data => data);

    }

    deleteComment(_id_annonce, _id_comment): Promise<any> {
        return this.http
            .post(`${this.serverHost}/feed/deleteComment`, {
                collection: 'annonce',
                _id_annonce: _id_annonce,
                _id_comment: _id_comment
            })
            .toPromise().then(data => data);

    }

    deleteUser(): Promise<any> {
        let _id_user = null;
        this.user.subscribe(u => {
            _id_user = u._id;
        });

        return this.http
            .post(`${this.serverHost}/user/delete`, {
                _id_user: _id_user,
            })
            .toPromise().then(data => data);
    }

    updateProfilePicture(imgUrl): Promise<any> {
        let _id_user;
        this.user.subscribe(u => _id_user = u._id);

        return this.http
            .post(`${this.serverHost}/user/profilePicture`, {
                _id_user: _id_user,
                imgUrl: imgUrl
            })
            .toPromise().then(data => data);
    }

    updateChat(_id_chat, text, _id_sender): Promise<any> {
        // console.log(text);
        return this.http
            .post(`${this.serverHost}/message/send`, {
                _id_chat: _id_chat,
                text: text,
                _id_sender: _id_sender
            })
            .toPromise().then(data => data);
    }

    updateComment(_id_annonce, _id_comment, updateComment): Promise<any> {
        return this.http
            .post(`${this.serverHost}/feed/updateComment`, {
                _id_annonce: _id_annonce,
                _id_comment: _id_comment,
                updateComment: updateComment
            })
            .toPromise().then(data => data);

    }

    updateAnnonce(_id, collection, value): Promise<any> {
        return this.http
            .post(`${this.serverHost}/crud/update`, {
                _id: _id,
                collection: collection,
                value: value
            })
            .toPromise().then(data => data);
    }

    updateReaction(_id_annonce, reaction, value): Promise<any> {

        return this.http
            .post(`${this.serverHost}/feed/updateReaction`, {
                _id_annonce: _id_annonce,
                reaction: reaction,
                value: value
            })
            .toPromise().then(data => data);
    }

    updateUser(value): Promise<any> {
        let _id_user = null;

        this.user.subscribe(u => {
            _id_user = u._id;
        });

        return this.http
            .post(`${this.serverHost}/crud/update`, {
                _id: _id_user,
                collection: 'user',
                value: value
            })
            .toPromise().then(data => data);
    }
}
