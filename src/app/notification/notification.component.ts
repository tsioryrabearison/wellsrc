import { Component, OnInit, Inject } from '@angular/core';
import { DataServices } from '../data.service';
import { DetailAnnonceComponent } from '../detail-annonce/detail-annonce.component';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss', '../feed/feed.component.scss']
})
export class NotificationComponent implements OnInit {
  notification: any;
  users: any;
  annonceUser: any;
  user: any;
  lang: string;
  db: any;
  constructor(
    private dts: DataServices,
    private router: Router,
    private dialogRef: MatDialogRef<NotificationComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {
    this.dts.getAllUsers().then(users => {
      this.users = users.body;
    });
    this.dts.user.subscribe(u => this.user = u);
    this.dts.getLang().subscribe(l => this.lang = l);
    this.dts.getDbLang().subscribe(db => this.db = db);
    // this.dts.getUsers().then(data => {
    //   this.utilisateur = data;
    // });
    // this.dts.getAnnonceUser(this.dts.User.id_user).then(data => {
    //   this.annonceUser = data;
    //   console.log(data);
    // });
    // this.dts.removeNotifAnnonce(this.dts.User.id_user).then(data => {
    //     console.log('remove');
    //     this.dts.refreshUser();
    // });
  }
  // findUser(id_user) {
  //   return this.utilisateur.find(user => user.id_user === id_user);
  // }
  // findAnnonce(id_annonce) {
  //   console.log(this.annonceUser);
  //   return this.annonceUser.find(annonce => annonce.id_annonce === id_annonce);
  // }
  // viewUser(id_user) {
  //   this.dialogRef.close();
  //   this.router.navigate(['/user-profil'], {
  //     queryParams: {
  //       id: id_user
  //     }
  //   });
  // }
  // viewAnnonce(id_annonce) {
  //   this.dialogRef.close();
  //   this.router.navigate(['/detail-annonce'], {
  //     queryParams: {
  //       id_annonce: id_annonce
  //     }
  //   });
  // }
  ngOnInit() {
  }
  findUser(_id_user) {
    return this.users.find(u => u._id === _id_user);
  }
  close() {
    this.dts.deleteNotif('annonce', null).then(data => {
      // this.dialogRef.close();
      this.data.callback();
    });
    this.dialogRef.close();
  }
}
