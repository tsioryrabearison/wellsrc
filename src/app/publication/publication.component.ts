import { Component, OnInit, Inject } from '@angular/core';
import { DataServices } from '../data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.scss', '../feed/feed.component.scss']
})
export class PublicationComponent implements OnInit {
  pics: any;
  annonce: FormGroup;
  user: any;
  lang: any;
  dbLang: any;
  saving: Boolean = false;
  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<PublicationComponent>,
    private dts: DataServices,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dts.getLang().subscribe(l => this.lang = l);
    this.dts.getDbLang().subscribe(db => this.dbLang = db);
    this.dts.user.subscribe(u => this.user = u);
    this.annonce = fb.group({
      '_id_user': this.user._id,
      'price_annonce': [null, Validators.compose([Validators.required, Validators.pattern(/^[0-9\/]+$/)])],
      'address_annonce': [null, Validators.compose([Validators.required, ])],
      'type_annonce': [null, Validators.required],
      'description_annonce': [null, Validators.required],
      'pic_annonce': [null, Validators.required],
      'reaction_annonce': [{
        follow: [],
        like: [],
        star: [],
        comment: [],
        available: true
      }, Validators.required]
      });
    this.pics = [];

  }
  ngOnInit() {
  }
  closeDialog() {
    this.dialogRef.close();
  }

  logForm() {
    console.log('log ...');
  this.saving = true;
   let counter = 0;
    const photos = [];
    const uploadFile = c => {
      const fileName = new Date().valueOf();
      console.log(this.pics[counter].blob);
      if (this.pics[counter].blob) {
        this.dts.getUrlFileUploaded(
          'pic_annonce/' + this.user._id,
          fileName,
          this.pics[c].blob
        ).then(res => {
          console.log(res);
          if (res.success) {
            if (counter > this.pics.length - 2) {
              photos.push(res.imgUrl);
              this.annonce.controls['pic_annonce'].setValue(photos);
              console.log('loglog');
              this.dts.save('annonce', this.annonce.value).then(() => {
                console.log('Publishing... ', this.annonce.value);
                console.log('calling callback');
                this.data.callback();
                this.closeDialog();
              });
            } else {
              photos.push(res.imgUrl);
              counter++;
              uploadFile(counter);
            }
          }
        });
      } else {
        console.log('error');
      }
    };
    uploadFile(counter);
  }
  fileChange (event, pic) {
    if (this.pics.length < 2) {
      // const idPic = pic.id;
      const reader = new FileReader();
      const file = event.target.files;
      if (file[0]) {
        if (file[0].type !== 'image/jpeg' && file[0].type !== 'image/png') {
        } else if (file[0].size > 25000000) {
          // opt.message = 'L\'image doit être inférieure à 25MB';
          // toast.present();
        } else {
          reader.readAsDataURL(file[0]);
          reader.onloadend = () => {
            // if (this.pics[idPic]) {
            //   console.log(this.pics[this.pics.length - 1].preview);
            //   this.pics[idPic].preview = reader.result;
            //   this.pics[idPic].blob = reader.result.split('base64,')[1].trim();
            //   if (this.pics.length < 5 && this.pics[this.pics.length - 1].preview) {
            //     this.pics.push({
            //       id: idPic + 1,
            //       preview: false,
            //       blob: false
            //     });
            //   }
            // } else {
            //   console.log('else');
            //   this.pics.push({
            //     id: idPic,
            //     preview: reader.result,
            //     blob: reader.result.split('base64,')[1].trim()
            //   });
            // }
            // console.log(this.pics);
          this.pics.push({
            preview: reader.result,
            blob: reader.result.toString().split('base64,')[1].trim()
          });

          // this.annonce.controls['pic_annonce'].setValue(file[0])
          };
        }
      }
    } else {
      alert('Vous pouvez au plus ajouter 2 photos! Pour ajouter plus de photo vous devez payer...');
    }
  }

}
