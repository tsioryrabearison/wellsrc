import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { DataServices } from '../data.service';
import { AlertComponent } from '../alert/alert.component';
import { MatDialog } from '@angular/material';
import { ModifAnnonceComponent } from '../modif-annonce/modif-annonce.component';

@Component({
  selector: 'app-detail-annonce',
  templateUrl: './detail-annonce.component.html',
  styleUrls: ['./detail-annonce.component.scss', '../feed/feed.component.scss']
})
export class DetailAnnonceComponent implements OnInit {
  trano: any;
  user: any;
  // userConnected: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dts: DataServices,
    private dialog: MatDialog
  ) {
    // // if (this.dts.User) {
    // //   this.userConnected = this.dts.User;
    // // }
    // this.route.queryParams.subscribe( params => {
    //   console.log(params);
    //   if (!params.id_annonce) {
    //           // --------------------------------REDIRECTION
    //     this.dts.user.subscribe((data_user) => {
    //       if (!data_user) {
    //           this.router.navigate(['/home']);
    //       } else {
    //         this.router.navigate(['/feed']);
    //       }
    //     });
    //   } else {
    //     this.dts.user.subscribe(data_user => {
    //       if (data_user) {
    //         this.user = data_user;
    //       }
    //     });
    //     this.dts.getAnnonce(params.id_annonce).then(data => {
    //       console.log(data);
    //       if (!data.success) {
    //         alert('erreur');
    //         if (!this.user) {
    //           this.router.navigate(['/feed']);
    //         } else {
    //           this.router.navigate(['/home']);
    //         }
    //       } else {
    //         this.trano = data.body[0];
    //         this.dts.getUserData(this.trano.id_user).then(data2 => {
    //           console.log(data2);
    //           // this.user = data2[0];
    //         });
    //       }
    //     });
    //   }
    // });
    // this.reaction = (reaction, idPublication) => {
    //   if (this.dts.User) {
    //     console.log(`${reaction} on pub ${idPublication}`);
    //     dts.getReaction(reaction, idPublication, this.dts.User.id_user).then(data => {
    //       console.log(data);
    //       this.dts.getAnnonceInfo(this.trano.id_annonce).then(data2 => {
    //         this.trano = data2[0];
    //         console.log(this.trano);
    //       });
    //     });
    //   } else {
    //     this.connectRequired();
    //   }
    //   // if (reaction === 'like') {
    //   //   // moins 1 car indice du tableau liste_trano commence par 0 or id commence par 1
    //   //   this.likeFunction(idPublication);
    //   // } else if ( reaction === 'comment') {
    //   //   // this.commentFunction(idPublication);
    //   // } else if ( reaction === 'follow') {
    //   //   this.followFunction(idPublication);
    //   // } else if ( reaction === 'rate') {
    //   //   this.rateFunction(idPublication);
    //   // }
    // };
    // // comment fuction
    // this.addComment = (forResize, texte, commentaireContainer, idPublication) => {
    //   if (texte.value !== '') {
    //     dts.getComment('comment', idPublication, this.dts.User.id_user, texte.value).then(data => {
    //       texte.value = '';
    //       this.resizeTextarea(forResize[0], forResize[1], forResize[2], forResize[3]);

    //       this.dts.getAnnonceInfo(this.trano.id_annonce).then(data2 => {
    //         this.trano = data2[0];
    //         console.log(this.trano);
    //         setTimeout(() => {
    //           commentaireContainer.scrollTo({top: commentaireContainer.scrollHeight});
    //         }, 10);
    //       });
    //     });
    //   }
    // };

   }

  ngOnInit() {
  }
  // openModifAnnonce() {
  //   console.log(this.trano.id_annonce);
  //   this.dialog.open(ModifAnnonceComponent, {
  //     data: {
  //       id_annonce: this.trano.id_annonce,
  //       callback: () => {
  //         this.dts.getAnnonceInfo(this.trano.id_annonce).then(dat2 => {
  //           this.trano = dat2[0];
  //         });
  //       }
  //     }
  //   });
  // }
  // addComment(forResize, texte, commentaireContainer, idPublication) {
  //   alert('Erreur! Veuillez reloader');
  // }
  // reaction(reaction, idPublication) {
  //   alert('Une erreur est survenue, nous vous prions de reloader.');
  // }
  // connectRequired() {
  //   this.dialog.open(AlertComponent, {
  //     data: {
  //       title: 'Fonctionnalité indisponible',
  //       text:  'Connectez-vous pour l\'activer',
  //     }
  //   });
  // }
  // goToChat(id_user) {
  //   if (this.dts.User) {
  //     this.router.navigate(['/chat'], {queryParams: {id_client: id_user}});
  //   } else {
  //     this.connectRequired();
  //   }
  // }
  // goToUserProfil(id_user) {
  //   if (this.dts.User) {
  //     this.router.navigate(['/user-profil'], {queryParams: {id: id_user}});
  //   } else {
  //     this.connectRequired();
  //   }
  // }
  // showComment(idPublication) {
  //   if (this.dts.User) {
  //     const commentPublication = document.getElementById(`comment${idPublication}`);
  //     const commentaireContainer = document.getElementById(`commentaires${idPublication}`);
  //     const indexHidden = commentPublication.className.indexOf('hidden');
  //     console.log('showComment' + commentPublication);
  //     if (indexHidden !== -1) {
  //       const part1Class = commentPublication.className.substring(0, indexHidden);
  //       const part2Class = commentPublication.className.substring(indexHidden + 7, commentPublication.className.length);
  //       commentPublication.className = part1Class + part2Class;
  //       setTimeout(() => {
  //         commentaireContainer.scrollTo({top: commentaireContainer.scrollHeight});
  //       }, 1);
  //     } else {
  //       commentPublication.className += ' hidden';
  //     }
  //   } else {
  //     this.connectRequired();
  //   }
  // }
  // calculeAge(date_birth) {
  //   let data_birthTable = date_birth.split('-');
  //   const data_birthTableInt = [];
  //   for (data_birthTable of data_birthTable) {
  //     data_birthTableInt.push(parseInt(data_birthTable, 10));
  //   }
  //   const date = new Date();
  //   const mois = date.getMonth();
  //   const anne = date.getFullYear();
  //   const jour = date.getDay();
  //   let age = anne - data_birthTableInt[2] - 1;
  //   if (mois >= data_birthTableInt[1]) {
  //     if (mois === data_birthTableInt[1]) {
  //       if (jour >= data_birthTableInt[0]) {
  //         age++;
  //       }
  //       } else {
  //           age++;
  //       }
  //   }
  //    return age;
  // }
  // resizeTextarea(textarea, textareaContainer, clone, idPublication) {
  //   let commentaireHeight;
  //   let textareaScrollHeight;
  //   let textareaHeight;

  //   if (document.getElementById(`commentaires${idPublication}`).style.height !== '') {
  //     commentaireHeight = parseInt(document.getElementById(`commentaires${idPublication}`).style.height, 10);
  //   } else {
  //     commentaireHeight = parseInt(getComputedStyle(document.getElementById(`commentaires${idPublication}`)).height, 10);
  //   }

  //   textareaContainer.style.height = '60px';
  //   clone.style.height = '60px';
  //   textareaScrollHeight = textarea.scrollHeight;
  //   textareaHeight = getComputedStyle(textarea, null).height;
  //   textareaContainer.style.height = (textareaScrollHeight + 20) + 'px';
  //   clone.style.height = (textareaScrollHeight + 20) + 'px';
  //   if (parseInt(textareaContainer.style.height, 10) + commentaireHeight > 400 &&  parseInt(textareaContainer.style.height, 10) < 224) {
  //     document.getElementById(`commentaires${idPublication}`).style.height = 400 - parseInt(textareaContainer.style.height, 10) + 'px';
  //   }
  // }

}
