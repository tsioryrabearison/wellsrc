import { TestBed, inject } from '@angular/core/testing';

import { DataServices } from './data.service';

describe('DataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataServices]
    });
  });

  it('should be created', inject([DataServices], (service: DataServices) => {
    expect(service).toBeTruthy();
  }));
});
