import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogOuiNonComponent } from './dialog-oui-non.component';

describe('DialogOuiNonComponent', () => {
  let component: DialogOuiNonComponent;
  let fixture: ComponentFixture<DialogOuiNonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogOuiNonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogOuiNonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
