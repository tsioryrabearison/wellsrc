import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataServices } from '../data.service';
@Component({
  selector: 'app-dialog-oui-non',
  templateUrl: './dialog-oui-non.component.html',
  styleUrls: ['./dialog-oui-non.component.scss', '../feed/feed.component.scss']
})
export class DialogOuiNonComponent implements OnInit {
  text: any;
  lang: any;
  db: any;
  constructor(
    private dialogRef: MatDialogRef<DialogOuiNonComponent>,
    private dts: DataServices,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dts.getLang().subscribe(l => this.lang = l);
    this.dts.getDbLang().subscribe(db => this.db = db);
    if ( data ) {
      this.text = data.text;
    }
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.closeDialog();
    this.data.ifAccepted();
  }
  refuse() {
    this.closeDialog();
    this.data.ifRefused();
  }
}
