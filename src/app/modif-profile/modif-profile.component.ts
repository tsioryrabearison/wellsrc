import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataServices } from '../data.service';
import { AlertComponent } from '../alert/alert.component';

@Component({
  selector: 'app-modif-profile',
  templateUrl: './modif-profile.component.html',
  styleUrls: ['./modif-profile.component.scss', '../feed/feed.component.scss']
})
export class ModifProfileComponent implements OnInit {
  rForm: FormGroup;
  constructor(
    private dialogRef: MatDialogRef<ModifProfileComponent>,
    private dialog: MatDialog,
    private dts: DataServices,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    switch (data.aModifier) {
        case 'name':
          this.rForm = fb.group({
            'firstName': [null, Validators.required],
            'lastName': [null, Validators.required]
          });
          break;
        case 'password':
          this.rForm = fb.group({
            'lastPassword': [null, Validators.required],
            'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            'verifPassword': [null, Validators.required]
          });
          break;
        case 'contact':
          this.rForm = fb.group({
            'phone': [null, Validators.compose([Validators.required, Validators.pattern(/^[\- 0-9]+$/)])],
          });
          break;
    }
  }

  ngOnInit() {
  }
  closeDialog() {
    this.dialogRef.close();
  }

  modifProfil(type) {
    console.log('okok');
    let donne;
    switch (type) {
      case 'name':
        donne = {
          firstName: this.rForm.value.firstName,
          lastName: this.rForm.value.lastName,
        };
        break;
      case 'password':
        donne = {
          password: this.rForm.value.password,
        };
        break;
      case 'contact':
        donne = {
          phone: this.rForm.value.phone
        };
        break;
    }
    this.dts.updateUser(donne).then(data2 => {
      console.log(donne);
      this.dialogRef.close();
      this.data.callback();
      this.dialog.open(AlertComponent, {
        data: {
          title: 'Modification terminée.'
        }
      });
    });
  }
}
