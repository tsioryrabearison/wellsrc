import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-privacy',
  templateUrl: './page-privacy.component.html',
  styleUrls: ['./page-privacy.component.scss']
})
export class PagePrivacyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onActivate(event) {
    console.log(event)
      window.scroll(0, 0);
  }
}
