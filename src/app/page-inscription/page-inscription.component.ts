import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataServices } from '../data.service';
import { DialogOuiNonComponent } from '../dialog-oui-non/dialog-oui-non.component';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { InscriptionModalComponent } from '../inscription-modal/inscription-modal.component';

@Component({
  selector: 'app-page-inscription',
  templateUrl: './page-inscription.component.html',
  styleUrls: ['./page-inscription.component.scss', '../feed/feed.component.scss']
})
export class PageInscriptionComponent implements OnInit {
  rForm: FormGroup;
  inscriptionEnCours = false;
  lang: string;
  db: any;
  constructor(private fb: FormBuilder,
    private dialog: MatDialog,
    private dts: DataServices,
    private router: Router
  ) {
    this.dts.getLang().subscribe(l => this.lang = l);
    this.dts.getDbLang().subscribe(db => this.db = db);
    this.rForm = fb.group({
      'username': [null, Validators.compose([Validators.required,
        Validators.minLength(4),
        Validators.pattern(/^[\w \-àäâîïûüùéèêëôöç]+$/)])],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      'passwordConfirm': [null, Validators.compose([Validators.required])],
      'firstName': [null, Validators.compose([Validators.required, Validators.pattern(/^[\w \-àäâîïûüùéèêëôöç]+$/)])],
      'lastName': [null, Validators.compose([Validators.required, Validators.pattern(/^[\w \-àäâîïûüùéèêëôöç]+$/)])],
      'date_birth': [null, Validators.compose([Validators.required, ])],
      'address': [null, Validators.compose([Validators.required, ])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern(/^[\- 0-9]+$/)])],
      'mail': [null, Validators.compose([Validators.required, Validators.email])],
      'pays': [null, Validators.required],
      'ville': [null, Validators.required],
      'terms': [null, Validators.required],
      'sexe': [null, Validators.required],
      });
  }
  addPost(data) {
    const login = {
      pseudo: data.username,
      password: data.password,
      _id_user: null
    };
    const post = {
      firstName: data.firstName,
      lastName: data.lastName,
      date_birth: data.date_birth,
      sexe: data.sexe,
      address_user: data.address,
      phone: data.phone,
      mail: data.mail,
      pays: data.pays,
      ville: data.ville,
      pic_prof: ['assets/img/default_pic_user.png'],
      status_user: {
        connected: false
      },
      notification: {
        annonce: [],
        chat: []
      }
    };
    // console.log(post);
        this.inscriptionEnCours = true;
        // this.dts.save('login', login).then(rep => {
          this.dts.save('user', post).then(dat => {
            // console.log('rep on save', dat);
            login._id_user = dat._id;
            this.dts.save('login', login).then(res => {
              this.dialog.open(DialogOuiNonComponent, {
                disableClose: true,
                autoFocus: false,
                data: {
                  text: {
                    title: this.lang === 'FR' ? '!Félicitations ! Vous êtes maintenant membre.' : 'Congratulations! You\'re now member',
                    subtitle: this.lang === 'FR ' ? 'se connecter ?' : 'Login ?'
                  },
                  ifAccepted: () => {
                    this.dialog.open(InscriptionModalComponent, {
                      width: 'auto',
                      height: 'auto',
                      data: {showText: false}
                    });
                  },
                  ifRefused: () => {
                    this.router.navigate(['/home']);
                  }
                }
              });
              this.inscriptionEnCours = false;
            });
            // ------------------- show dialog to connect or not
            // const dialogConfig = new MatDialogConfig();
            // dialogConfig.disableClose = false;
            // dialogConfig.autoFocus = true;
            // this.dialog.open(DialogOuiNonComponent, dialogConfig);
          // });
        });

  }
  ngOnInit() {
  }

}
