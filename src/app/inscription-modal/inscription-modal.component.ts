import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DataServices } from '../data.service';
import { BehaviorSubject } from 'rxjs';
import { Socket } from 'ng-socket-io';

import { LoadingComponent } from '../loading/loading.component';
import { FeedComponent } from '../feed/feed.component';

@Component({
  selector: 'app-inscription-modal',
  templateUrl: './inscription-modal.component.html',
  styleUrls: ['./inscription-modal.component.scss']
})
export class InscriptionModalComponent implements OnInit {

  showText: Boolean;
  checkLog = true;
  loading = false;
  lang: string;
  dbLang: any;
  constructor(public dialogRef: MatDialogRef<InscriptionModalComponent>,
    private dts: DataServices,
    private dialog: MatDialog,
    private router: Router,
    private socket: Socket,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dts.Lang.subscribe(lang => this.lang = lang);
    this.dts.dbLang.subscribe(dbLang => this.dbLang = dbLang);
    this.showText = data.showText;
  }

  ngOnInit() {
  }
  closeInscription(): void {
    this.dialogRef.close();
    // console.log('Close dialog.');
  }
  // ------------------------------
  checkLogin(username, password) {
    // console.log(username + '=>' + password);
    this.loading = true;
    this.dts.login(username, password).then(data => {
      if (data.success) {
        this.dts.getUserData(data._id_user).then(user => {
          if (this.dts.user) {
              // seting local storage key 'llew_resu' (well_user)
              console.log('setting local')
              localStorage.setItem('llew_resu', user.body[0]._id);

              this.dts.user.next(user.body[0]);
              const _id_user = user.body[0]._id;
              // this.dts.user.subscribe((u: any) => {
              //     _id_user = u._id;
              // });
              this.socket.emit('logged-in', _id_user);
            } else {
              this.dts.user = new BehaviorSubject(user.body[0]);

              // seting local storage key 'llew_resu' (well_user)
              localStorage.setItem('llew_resu', user.body[0]._id);
          }
          this.closeInscription();
          // this.dts.user.subscribe(u => // console.log());
              this.router.navigate(['/feed']);
        });
      } else {
        this.loading = false;
        this.checkLog = false;
        // console.log(data);
      }
    });
  }

}
