import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';
// import { Http } from '@angular/http';

import { DataServices } from './data.service';
import { UserService } from './user.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
// import { InscriptionComponent } from './inscription/inscription.component';
import { FeedComponent } from './feed/feed.component';
import { PublicationComponent } from './publication/publication.component';
// import { RechercheComponent } from './recherche/recherche.component';
// import { ChatRomComponent } from './chat-rom/chat-rom.component';
import { MessagesComponent } from './messages/messages.component';
import { NotificationComponent } from './notification/notification.component';
import { InscriptionModalComponent } from './inscription-modal/inscription-modal.component';
import {NgbModule, NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';


import { FormsModule, ReactiveFormsModule, } from '@angular/forms';
import { ParallaxModule, ParallaxConfig } from 'ngx-parallax';
import { MaterialModule } from './material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { SliderModule } from 'angular-image-slider';

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { PageInscriptionComponent } from './page-inscription/page-inscription.component';
import { PageFaqComponent } from './page-faq/page-faq.component';
import { PageAboutComponent } from './page-about/page-about.component';
// import { DetailAnnonceComponent } from './detail-annonce/detail-annonce.component';
import { PagePrivacyComponent } from './page-privacy/page-privacy.component';
import { PageTermsComponent } from './page-terms/page-terms.component';
import { DialogOuiNonComponent } from './dialog-oui-non/dialog-oui-non.component';
import { AlertComponent } from './alert/alert.component';
// import { ResultComponent } from './result/result.component';
import { UserProfilComponent } from './user-profil/user-profil.component';
import { ModifAnnonceComponent } from './modif-annonce/modif-annonce.component';
import { ModifProfileComponent } from './modif-profile/modif-profile.component';
import { CommentComponent } from './comment/comment.component';
import { ShowPicAnnonceComponent } from './show-pic-annonce/show-pic-annonce.component';
import { AnnoncesComponent } from './annonces/annonces.component';
import { ResultComponent } from './result/result.component';
// import { LoadingComponent } from './loading/loading.component';

const config: SocketIoConfig = {
  url: 'https://server.wellcoloc.com/',
  options: {transports: ['websocket', 'polling']}
};
// const config: SocketIoConfig = {
//   url: 'http://localhost:4000/',
//   options: {transports: ['websocket', 'polling']}
// };

const  ROUTES: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'feed', component: FeedComponent},
  {path: 'search', component: ResultComponent},
  // {path: 'publication', component: PublicationComponent},
  {path: 'chat', component: MessagesComponent},
  {path: 'inscription', component: PageInscriptionComponent},
  { path: 'about', component: PageAboutComponent },
  { path: 'terms-and-conditions', component: PageTermsComponent },
  { path: 'faq', component: PageFaqComponent },
  { path: 'privacy-policy', component: PagePrivacyComponent },
  // { path: 'detail-annonce', component: DetailAnnonceComponent},
  { path: 'user-profil', component: UserProfilComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    // InscriptionComponent,
    FeedComponent,
    PublicationComponent,
    // RechercheComponent,
    // ChatRomComponent,
    MessagesComponent,
    NotificationComponent,
    InscriptionModalComponent,
    PageInscriptionComponent,
    PageFaqComponent,
    PageAboutComponent,
    PagePrivacyComponent,
    PageTermsComponent,
    DialogOuiNonComponent,
    AlertComponent,
    // ResultComponent,
    // DetailAnnonceComponent,
    UserProfilComponent,
    ModifAnnonceComponent,
    ModifProfileComponent,
    ShowPicAnnonceComponent,
    // LoadingComponent,
    CommentComponent,
    AnnoncesComponent,
    ResultComponent
  ],
  imports: [
    BrowserModule,
    // MDBBootstrapModule.forRoot(),
    NgbModule,
    SliderModule,
    HttpClientModule,
    MaterialModule,
    Angular2FontawesomeModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ParallaxModule,
    SocketIoModule.forRoot(config),
    RouterModule.forRoot(ROUTES, {useHash: true})
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [DataServices, UserService, NgbCarouselConfig],
  bootstrap: [AppComponent],
  entryComponents: [
    PublicationComponent,
    NotificationComponent,
    InscriptionModalComponent,
    DialogOuiNonComponent,
    AlertComponent,
    ResultComponent,
    ModifAnnonceComponent,
    ModifProfileComponent,
    ShowPicAnnonceComponent,
    // LoadingComponent,
    CommentComponent,
  ]
})
export class AppModule {

}
