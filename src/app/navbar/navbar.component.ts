import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { RechercheComponent } from '../recherche/recherche.component';
import { PublicationComponent } from '../publication/publication.component';
import { NotificationComponent } from '../notification/notification.component';
import { InscriptionModalComponent } from '../inscription-modal/inscription-modal.component';
import { DialogOuiNonComponent } from '../dialog-oui-non/dialog-oui-non.component';
import { DataServices } from '../data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss', './../feed/feed.component.scss']
})
export class NavbarComponent implements OnInit {
  links = [
    {'name': 'S\'inscrire', 'icon': 'location_city'},
    {'name': 'Se connecter', 'icon': 'account_circle'}
  ];
  user: any;
  login: any;
  dbLang: any;
  lang: any;
  constructor (private dialog: MatDialog, private dts: DataServices, private route: Router) {

    dts.getLang().subscribe(data => this.lang = data);
    dts.getDbLang().subscribe(data => this.dbLang = data);
    this.dts.user.subscribe(u => this.user = u);
    if (!this.user._id && localStorage.getItem('llew_resu')) {
      this.dts.getUserData(localStorage.getItem('llew_resu')).then(data => {
        this.dts.user.next(data.body[0]);
      });
    }
  }

  setLanguage(e) {
    this.dts.setLang(e.value);
  }
  openInscription(showText): void {
    const dialogRef = this.dialog.open(InscriptionModalComponent, {
      height: 'auto',
      width: 'auto',
      data: { showText: showText }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

    openDialogNotification() {
      this.dialog.open(NotificationComponent, {
        maxWidth: '600px',
        maxHeight: window.innerHeight - 15 + 'px',
        disableClose: false,
        autoFocus: true,
        data: {
          callback: () => {
            this.dts.getUserData(this.user._id).then(user => {
              if (this.dts.user) {
                  this.dts.user.next(user.body[0]);
                  this.dts.user.subscribe(u => this.user = u);
                }
            });
          }
        }
      });
    }
    goToUserProfil(id_user) {
      this.route.navigate(['/user-profil'], {queryParams: {id: id_user}});
    }

    goToChat() {
      this.dts.deleteNotif('chat', null).then(() => {
        this.dts.getUserData(this.user._id).then(user => {
          if (this.dts.user) {
              this.dts.user.next(user.body[0]);
              this.dts.user.subscribe(u => this.user = u);
              this.route.navigate(['/chat']);
            }
        });
      });
    }
  ngOnInit() {
  }
  logout() {
    this.dialog.open(DialogOuiNonComponent, {
      data: {
        text: {
          title: this.dbLang.confirm_deconnection[this.lang]
        },
        ifAccepted: () => {
          this.dts.logout().then(() => {
            localStorage.clear();
            console.log('in then');
            this.dts.user.next({});
            this.dts.user.subscribe(u => this.user = u);
            this.route.navigate(['/home']);
          });
        },
        ifRefused: () => {
        }
      }
    });
  }
  hideElem(elem: any) {
    elem.style.transform = 'scale(0)';
    elem.style.opacity = '0';
  }
  showElem(elem: any) {
    elem.style.transform = 'scale(1)';
    elem.style.opacity = '1';
  }
    // if (linkname === `S'inscrire`) {
    //   this.links = [
    //     {'name': 'Home', 'icon': 'home'},
    //     {'name': 'Message', 'icon': 'chat'},
    //     {'name': 'Notification', 'icon': 'notifications'},
    //     {'name': 'Déconnexion', 'icon': 'power_settings_new'}
    //   ];
    //   this.login = true;
    //   console.log('logged');
    // } else if (linkname === 'Déconnexion') {
    //   this.links = [
    //     {'name': 'S\'inscrire', 'icon': 'location_city'},
    //     {'name': 'Se connecter', 'icon': 'account_circle'}
    //   ];
    //   this.login = false;
    // } else if (linkname === 'Publier') {
    //   this.openDialogPublication();
    // } else if (linkname === 'Notification') {
    //   this.openDialogNotification();
    // } else if (linkname === 'Home') {
    //   this.route.navigate(['/feed']);
    // }

  }
  // trackByFn(index, item) {
  //   return index;
  // }

