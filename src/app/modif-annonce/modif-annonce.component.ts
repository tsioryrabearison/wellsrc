import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataServices } from '../data.service';
import { AlertComponent } from '../alert/alert.component';
@Component({
  selector: 'app-modif-annonce',
  templateUrl: './modif-annonce.component.html',
  styleUrls: ['./modif-annonce.component.scss', '../feed/feed.component.scss']
})
export class ModifAnnonceComponent implements OnInit {
  rForm: FormGroup;
  lang: string;
  db: any;
  constructor(
    private dialogRef: MatDialogRef<ModifAnnonceComponent>,
    private dialog: MatDialog,
    private dts: DataServices,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    console.log(data);
    this.dts.getLang().subscribe(l => this.lang = l);
    this.dts.getDbLang().subscribe(db => this.db = db);
    this.rForm = fb.group({
      'price': [null, Validators.required],
      'type': [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  modifAnnonce(donne) {
    console.log(this.data);
    this.dts.updateAnnonce(this.data.id_annonce, 'annonce', {
        price_annonce: donne.price,
        type_annonce: donne.type
    }).then( dat => {
      console.log(dat);
      this.closeDialog();
      this.data.callback();
      this.dialog.open(AlertComponent, {
        data: {
          title: this.lang==='FR' ? 'Modification terminée.' : 'Success'
        }
      });
    });
  }

}
