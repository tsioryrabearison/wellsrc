 import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  private isUserLogged;
  private user;
  constructor() {
    this.isUserLogged = false;
   }
  setUserLoggedIn(etat) {
    this.isUserLogged = etat;
  }
  getUserLoggedIn() {
    return this.isUserLogged;
  }


}
