import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { DataServices } from '../data.service';
import * as $ from 'jquery';
import { Socket } from 'ng-socket-io';
import * as moment from 'moment';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss', '../feed/feed.component.scss']
})
export class MessagesComponent implements OnInit {
  users: any;
  user: any;
  chats: any;
  show_chat: any;
  _id_user_on_chat: any;
  _id_chat_on_view: any;
  chat_on_views: any;
  db: any;
  lang: any;
  changing_chat: Boolean = false;
  _id_client_params: string;
  // utilisateurs: any;
  // User: any;
  // user: any;
  // chatBox: any;
  // chat: any;
  // chatOnViewIndex: any;
  // test: any;
  // messageFind = false;
  // loading = false;
  // Client = {
  //   'id_user': -1
  // };
  // newChat = [
  //   'Aucune message trouver'
  // ];
  // constructor( private dts: DataServices, private route: ActivatedRoute , private socket: Socket, private router: Router) {
  //   this.dts.user.subscribe(u => this.user = u);
  // //   // --------------------------------REDIRECTION
  // //   if (!this.dts.User) {
  // //     this.router.navigate(['/home']);
  // //   }
  // //   socket.on('socketUpdateMessage', (_data) => {
  // //     // // console.log(_data);
  // //     const indexChat = this.chat.findIndex(chat => chat.id_chat === _data.id_chat);
  // //     // // console.log(indexChat);
  // //     if (indexChat !== -1 && _data.id_sender !== this.dts.User.id_user) {
  // //       document.getElementById('sup-' + _data.id_sender).style.background =  'yellow';
  // //     }
  // //   });
  //   this.getAllUser();
  // //     // ---------------------------GET USER INFO
  //       this.dts.getAllChat().then(data => {
  //         // // console.log(data);
  //         // // console.log('here get all chat');

  //         this.chat = (data.body.length === 0) ? [ {
  //           _id: -1,
  //           id_user: this.user._id,
  //           id_client: -1,
  //           message: [
  //             {
  //               date_send: 'now',
  //               id_sender: -1,
  //               text: 'Bienvenue dans la page de discution sur wellcoloc !'
  //             }
  //           ]
  //         }] : data;

  //         this.messageFind = true;
  //         this.route.queryParams.subscribe(params => {
  //           if (params.id_client) {
  //             this.Client.id_user = parseInt(params.id_client, 10);
  //             this.changeMessageOnView(this.Client.id_user);
  //           } else {
  //             this.Client.id_user =  (this.chat[0].id_user === this.dts.User.id_user ? this.chat[0].id_client : this.chat[0].id_user);
  //             this.chatOnViewIndex = 0;
  //             if (this.dts.User.notification.message.indexOf(this.Client.id_user) !== -1) {
  //               this.dts.removeNotifMessage(this.dts.User.id_user, this.Client.id_user).then(result => {
  //                     this.dts.User.notification.message.splice(this.dts.User.notification.message.indexOf(this.chatOnViewIndex));
  //               });
  //             }
  //           }
  //         //   if (data.length > 0) {
  //         //     this.Client.id_user = (this.dts.User.id_user === this.chat[this.chatOnViewIndex].id_user) ? // -same line
  //         //   this.chat[this.chatOnViewIndex].id_client : this.chat[this.chatOnViewIndex].id_user; // -same line}
  //         // // // console.log('chat loaded');
  //         // }
  //         });
  //       });
  // }

  // ngOnInit() {
  // }
  // // // ---------------------------GET ALL USER
  // getAllUser() {
  //      this.dts.getAllUsers().then(data =>  {
  //       this.utilisateurs = data.body;
  //       this.utilisateurs.push({
  //         _id: -1,
  //         firstName: 'Wellcoloc',
  //         lastname: '',
  //         pic_prof: [
  //           'assets/img/welllogo.png'
  //         ],
  //         status_user: {
  //           connected: false
  //         }
  //       });
  //     });
  // }
  // // // ---------------------------GET CHAT OF USER
  // // getUserChat() {
  // //   // // console.log(this.dts.User.id_user);
  // //   this.dts.getAllChat(this.dts.User.id_user).then(dataChat => {
  // //     if (dataChat.length > 0) {
  // //       this.chat = dataChat;
  // //     }
  // //     // // console.log('chat: ', dataChat);
  // //     this.ngAfterViewInit();
  // //   });
  // // }
  // // findUserOnChat() {
  // //   if (this.messageFind) {
  // //     const idUserToFind = (this.chat[this.chatOnViewIndex].id_user === this.dts.User.id_user) ?
  // //     this.chat[this.chatOnViewIndex].id_client : this.chat[this.chatOnViewIndex].id_user;
  // //     return this.utilisateurs.find(user => user.id_user === idUserToFind);
  // //   } else {
  // //     return this.utilisateurs.find(user => user.id_user === this.Client.id_user);
  // //   }

  // // }
  // changeMessageOnView(id_user) {
  //   // // console.log(id_user + 'change');
  //   const indexMessage = this.chat.findIndex(chat => chat.id_user === id_user || chat.id_client === id_user);
  //   if (indexMessage !== -1) {
  //     this.messageFind = true;
  //     this.chatOnViewIndex = indexMessage;
  //     if (this.user.notification.message.length > 0 && this.user.notification.message.indexOf(id_user) !== -1) {
  //       alert(this.user.notification.message);
  //       // this.dts.removeNotifMessage(this.dts.User.id_user, this.Client.id_user).then(data => {
  //       //   this.dts.User.notification.message.splice(this.dts.User.notification.message.indexOf(this.chatOnViewIndex));
  //       // });
  //     }
  //     if (this.user.notification.message.indexOf(this.chatOnViewIndex) !== -1) {
  //       // this.dts.removeNotifMessage(this.dts.User.id_user, this.chatOnViewIndex).then(result => {
  //         // this.dts.User.notification.message.splice(this.dts.User.notification.message.indexOf(this.chatOnViewIndex));
  //       // });
  //     }


  //     // // console.log(this.chat[this.chatOnViewIndex]);
  //     this.Client.id_user = (this.user.id_user === this.chat[this.chatOnViewIndex].id_user) ? // -same line
  //     this.chat[this.chatOnViewIndex].id_client : this.chat[this.chatOnViewIndex].id_user; // -same line

  //     this.ngAfterViewInit();
  //   } else {
  //       this.Client.id_user = id_user;
  //       this.messageFind = false;
  //   }
  // }
  // // sendMessage(messageArea) {
  // //   // // console.log(this.Client.id_user + ' client');
  // //   if (messageArea.value !== '' && this.messageFind && !this.loading) {
  // //     // // console.log(this.chat[this.chatOnViewIndex].id_chat + 'id_chat');
  // //     // // console.log('here messfind');
  // //     this.loading = true;
  // //     const message = messageArea.value;
  // //     const date = new Date();
  // //     const newMessObject = {
  // //         'date_send': date.getTime(),
  // //         'text': message,
  // //         'id_sender': this.dts.User.id_user
  // //       };
  // //       const id_chat = this.chat[this.chatOnViewIndex].id_chat;
  // //       this.dts.updateChat(message, id_chat, this.dts.User.id_user, this.Client.id_user).then(data => {
  // //         this.dts.getAllChat(this.dts.User.id_user).then(dataChat => {
  // //           this.chat = dataChat;
  // //           this.loading = false;
  // //         });
  // //       });
  // //      this.chat[0].message.push(newMessObject);
  // //     setTimeout(() => {
  // //     //  const messageContainer = document.getElementById('messageContainer');
  // //     //  messageContainer.scrollTo({top: messageContainer.scrollHeight});
  // //     this.ngAfterViewInit();
  // //      messageArea.value = '';
  // //     }, 100);

  // //   } else if (messageArea.value !== '' && !this.messageFind && !this.loading) { // ---new chat
  // //     // // console.log('here not find');
  // //     this.loading = true;
  // //     const date = new Date();
  // //     const message = messageArea.value;
  // //     const dataToSend = {
  // //       id_user: this.dts.User.id_user,
  // //       id_client: this.Client.id_user,
  // //       id_sender: this.dts.User.id_user,
  // //       message: [
  // //         {
  // //             date_send: date.getTime(),
  // //             text: message,
  // //             id_sender: this.dts.User.id_user
  // //         }
  // //       ]
  // //     };
  // //       this.dts.newChat(dataToSend).then(data => {
  // //         this.dts.getAllChat(this.dts.User.id_user).then(dataChat => {
  // //           // // console.log(dataChat);
  // //           this.chat = dataChat;
  // //           this.messageFind = true;
  // //           this.chatOnViewIndex = this.chat
  // //           .findIndex(chat => chat.id_user === this.Client.id_user || chat.id_client === this.Client.id_user);
  // //           // // console.log(this.chatOnViewIndex + 'chatonview index');
  // //           setTimeout(() => {
  // //             messageArea.value = '';
  // //             this.loading = false;
  // //            }, 100);
  // //         });
  // //       });

  // //   }

  // //  }
  // // resizeTextarea(textarea, textareaContainer) {
  // //     let textareaScrollHeight;
  // //     let textareaHeight;
  // //     textareaContainer.style.height = '60px';
  // //     textareaScrollHeight = textarea.scrollHeight;
  // //     textareaHeight = getComputedStyle(textarea, null).height;
  // //       textareaContainer.style.height = (textareaScrollHeight + 20) + 'px';
  // //       if (parseInt(textareaContainer.style.height, 10) < 314) {
  // //         const height = parseInt(textareaContainer.style.height, 10) + 'px';
  // //         if (document.getElementById('messageContainer')) {
  // //           document.getElementById('messageContainer').style.minHeight = 'calc(100% - ' + height  + ')';
  // //           document.getElementById('messageContainer').style.height = 'calc(100% - ' + height  + ')';
  // //         } else {
  // //           document.getElementById('messageContainerClone').style.minHeight = 'calc(100% - ' + height  + ')';
  // //           document.getElementById('messageContainerClone').style.height = 'calc(100% - ' + height  + ')';
  // //         }
  // //       }
  // // }
  // // findChat(id) {
  // // }
  // // trackByFn(index, item) {
  // //   return index;
  // // }


  /**************************************************************************** */
  // constructor( private dts: DataServices, private route: ActivatedRoute, private router: Router, private socket: Socket,
  //   ) {

  //   };

  constructor( private dts: DataServices, private route: ActivatedRoute, private router: Router, private socket: Socket,
     ) {

       // checking for existance of user
        this.dts.user.subscribe(u => this.user = u);
            if (!this.user._id && !localStorage.getItem('llew_resu')) { // llew_resu => user_well (read right to left)

                // user not exist => redirection to home
                this.router.navigate(['/home']);

            } else {

                // user exist on localstorage or at dataservice
                this.dts.user.subscribe((u: any) => this.user = u);
                if (!this.user._id) { // if user is not at dataservice, certainly it's in localstorage (it's logic)

                    // passing the localstorage user to dataservice and to this class
                    this.dts.getUserData(localStorage.getItem('llew_resu')).then((data: any) => {
                        this.dts.user.next(data.body[0]);
                        this.dts.user.subscribe((u: any) => this.user = u);
                        this.socket.emit('logged-in', this.user._id);

                    });
                }
            }
          // ***** end of checking existance user

          // ------------------------socket

          this.socket.on('socketUpdateChat', (_data) => {
            console.log('chattttt');
            const  _id_chat = this._id_chat_on_view;
            this.dts.getChat(_id_chat, null).then(chats => {
              this.chat_on_views.messages = chats.body[0].messages;
              this.ngAfterViewInit();
            });
          });


    // the messages script beging here

          // set language
          this.dts.getLang().subscribe((l: string) => this.lang = l);
          this.dts.getDbLang().subscribe((db: any) => this.db = db);

          // set the params id_client even if it's not exist
          this.route.queryParams.subscribe((params: any) => this._id_client_params = params.id_client);

          // getting all users and affect it in {{ users }} (intuitive like the name of methode)
          this.dts.getAllUsers().then((data_users: any) => {
              this.users = data_users.body;

              // getting all chat of user and affect it in {{ chats }}
              this.dts.getAllChat().then((data_chats: any) => {
                  this.chats = data_chats.body ? data_chats.body : [];
                  // console.log('chats', this.chats)
                      // set the id of client who is at the dialog message {{ id_user_on_chat }}
                      if (this._id_client_params) {
                        this._id_user_on_chat = this._id_client_params;
                      } else {
                        let id_client: string ;
                        if (this.chats.length > 0) {
                          id_client = this.chats[0].client[0]._id === this.user._id ?
                          this.chats[0].client[1]._id : this.chats[0].client[0]._id;
                        } else {
                        }
                        this._id_user_on_chat = id_client;
                        // console.log('hoho,', this._id_user_on_chat);
                      }

                      // find chat of user and client
                      const chat_client_user: any = this.chats.find((chat: any) => {
                        return chat && (chat.client[0]._id === this._id_user_on_chat || chat.client[1]._id === this._id_user_on_chat);
                      });
                      // console.log('client', chat_client_user);
                      if (chat_client_user) {
                        // console.log('testttt', chat_client_user);
                        this.chat_on_views = chat_client_user;
                        this._id_chat_on_view = chat_client_user._id;
                        // scrolling message
                        this.ngAfterViewInit();
                        // console.log(this._id_chat_on_view);
                        const timer = setInterval(() => {
                          if (this.chats[0] && this.chats[0].messages && $('#i' + this._id_user_on_chat)) {
                            $('#i' + this._id_user_on_chat).addClass('active');
                            clearInterval(timer);
                          }
                        }, 10);
                      } else {
                        // console.log('in else', this._id_user_on_chat);
                        if (this._id_user_on_chat) {
                          this.dts.getChat(null, this._id_user_on_chat).then((data_chat: any) => {
                            if (!this.chats[0]) {
                              this.chats[0] = {
                                _id: data_chat._id_chat,
                                client: [{
                                  _id: this.user._id
                                }, {
                                  _id: this._id_user_on_chat
                                }],
                                messages: []
                              };
                            }
                            // console.log('id_chat', data_chat);
                            this.chat_on_views = data_chat.body ? data_chat.body[0] : {
                                _id: data_chat._id_chat,
                                client: [{
                                  _id: this.user._id
                                },{
                                  _id: this._id_user_on_chat
                                }],
                                messages: []
                              };
                            // scrolling message
                            // console.log(this.chat_on_views);
                            this._id_chat_on_view = this.chat_on_views._id;
                            // console.log(this._id_chat_on_view);

                            this.ngAfterViewInit();
                            // console.log(this._id_chat_on_view);
                            const timer = setInterval(() => {
                              if (this.chats[0] && this.chats[0].messages && $('#i' + this._id_user_on_chat)) {
                                $('#i' + this._id_user_on_chat).addClass('active');
                                clearInterval(timer);
                              }
                            }, 10);
                          });
                        }

                      }
              });


          });



    }
//       this.dts.user.subscribe(u => this.user = u);
//       if (!this.user._id && !localStorage.getItem('llew_resu')) {

//         this.router.navigate(['/home']);

//       } else {

//           this.dts.getLang().subscribe(l => this.lang = l);
//           this.dts.getDbLang().subscribe(db => this.db = db);
//           if (!this.user._id) {
//             this.dts.getUserData(localStorage.getItem('llew_resu')).then(data => {
//               this.dts.user.next(data.body[0]);
//               this.dts.user.subscribe(u => this.user = u);
//             });
//             this.socket.on('socketUpdateChat', (_data) => {
//               // // console.log('socket reçue');
//               const  _id_chat = this._id_chat_on_view;
//               this.dts.getChat(_id_chat, null).then(chats => {
//                 this.chat_on_views.messages = chats.body[0].messages;
//                 this.ngAfterViewInit();
//               });
//             });
//             this.dts.user.subscribe(u => this.user = u);
//             this.dts.getAllUsers().then(data => {
//             this.users = data.body;
// =>          let _id_client = this.users[0]._id === this.user._id ? this.users[1]._id : this.users[0]._id;
//             this._id_user_on_chat = _id_client;
//             this.dts.getAllChat().then(chats => {
//               this.chats = chats.body ? chats.body : [];
//               // this.show_chat = this.chats[0] ? this.dts : null;
//               if (!this.chats[0]) {
//                   this.route.queryParams.subscribe((params: any) => {
//                     // console.log(params.id_client);
//                     if (params.id_client) {
//                       _id_client = params.id_client;
//                     } else {
//                       _id_client = this.users[0]._id === this.user._id ? this.users[1]._id : this.users[0]._id;
//                     }
//                   });
//                   this._id_user_on_chat = _id_client;
//                   // // console.log(_id_client);
//                   this.dts.getChat(null, _id_client).then(chat => {
//                     // // console.log(chat);
//                     this._id_chat_on_view = chat._id_chat;
//   =>                  this.chat_on_views = {messages: []};
//                     // // console.log('on views', this.chat_on_views);
//                   });
//               } else {
//                 this.route.queryParams.subscribe((params: any) => {
//                   if (params.id_client) {
//                     this.change_chat(params.id_client);
//                     // console.log(params.id_client);
//                   } else {
//                     this._id_chat_on_view = this.chats[0]._id;
//                     this._id_user_on_chat = this.chats[0].client[0]._id === this.user._id ?
//                     this.chats[0].client[1]._id : this.chats[0].client[0]._id;
//                     this.dts.deleteNotif('chat', this._id_user_on_chat).then(notif => {
//                       // this.dialogRef.close();
//                       this.dts.getUserData(this.user._id_user).then(user => {
//                         if (this.dts.user) {
//                             this.dts.user.next(user.body[0]);
//                           }
//                       });
//                     });
//                     // // console.log(this._id_user_on_chat);
//                     this.chat_on_views = this.chats[0];
//                     // const timer = setInterval(() => {
//                     //   if (this.chats[0].messages && $('#i' + this._id_user_on_chat)) {
//                     //     $('#i' + this._id_user_on_chat).addClass('active');
//                     //     clearInterval(timer);
//                     //   }
//                     // }, 10);
//                   }
//                 });
//               }
//             // // console.log(this.chats);
//             });

//         // // console.log(this.users);
//       });
//       } else {
//         this.socket.on('socketUpdateChat', (_data) => {
//           // // console.log('socket reçue');
//           const  _id_chat = this._id_chat_on_view;
//           this.dts.getChat(_id_chat, null).then(chats => {
//             this.chat_on_views.messages = chats.body[0].messages;
//             this.ngAfterViewInit();
//           });
//         });
//     this.dts.user.subscribe(u => this.user = u);
//     this.dts.getAllUsers().then(data => {
//     this.users = data.body;
//     let _id_client;
//     this.route.queryParams.subscribe((params: any) => {
//       if (params.id_client) {
//         _id_client = params.id_client;
//       } else {
//         _id_client = this.users[0]._id === this.user._id ? this.users[1]._id : this.users[0]._id;
//       }

//     });
//     this._id_user_on_chat = _id_client;
//     this.dts.getAllChat().then(chats => {
//       this.chats = chats.body ? chats.body : [];
//       // this.show_chat = this.chats[0] ? this.dts : null;
//       if (!this.chats[0]) {
//           this._id_user_on_chat = _id_client;
//           // // console.log(_id_client);
//           this.dts.getChat(null, _id_client).then(chat => {
//             // // console.log(chat);
//             this._id_chat_on_view = chat._id_chat;
//             this.chat_on_views = {messages: []};
//             // // console.log('on views', this.chat_on_views);
//           });
//       } else {
//         this._id_chat_on_view = this.chats[0]._id;
//         this._id_user_on_chat = this.chats[0].client[0]._id === this.user._id ?
//         this.chats[0].client[1]._id : this.chats[0].client[0]._id;
//         this.dts.deleteNotif('chat', this._id_user_on_chat).then(notif => {
//           // this.dialogRef.close();
//           this.dts.getUserData(this.user._id_user).then(user => {
//             if (this.dts.user) {
//                 this.dts.user.next(user.body[0]);
//               }
//           });
//         });
//         // // console.log(this._id_user_on_chat);
//         this.chat_on_views = this.chats[0];
//         // const timer = setInterval(() => {
//         //   if (this.chats[0].messages && $('#i' + this._id_user_on_chat)) {
//         //     $('#i' + this._id_user_on_chat).addClass('active');
//         //     clearInterval(timer);
//         //   }
//         // }, 10);

//      }
//     // // console.log(this.chats);
//     });
//     // // console.log(this.users);
//     });
//     }
//   }
// }

  ngOnInit() {

  }
  getTime(date) {
    return moment(date).locale(this.lang.toLowerCase()).format('LLL');
  }
  loadingAnimation() {
    const loadingDot = [ '.', '..', '...'];
    let index = 0;
    const loading = setInterval(() => {
      if (!this.chats || !this.users) {
        const loadingComponent = document.getElementById('loading');
        if (loadingComponent) {
          loadingComponent.innerText = loadingDot[index];
          index = index >= 2 ? 0 : index + 1;
        }
      }
    }, 800);
    setTimeout(() => {
      if (this.chats && this.users) {
        clearInterval(loading);
      }
    }, 2500);
  }
  ngAfterViewInit(): void {
    const scrollmess = setInterval(() => {
      const messageContainer = document.getElementById('messages_content');
      if (messageContainer)  {
        messageContainer.scrollTo({top: messageContainer.scrollHeight});
        clearInterval(scrollmess);
      }
    }, 10);

  }
  change_chat(id) {
    // // console.log('in change');
    // // console.log(id);
    // console.log('chh', id);
    $('.active').removeClass('active');
    if (!$('#i' + id)) {
       const time = setInterval(() => {
          if ($('#i' + id)) {
            $('#i' + id).addClass('active');
            clearInterval(time);
          }
       }, 30);
    } else {
      $('#i' + id).addClass('active');
    }
    // // console.log(this.chats);
    this._id_user_on_chat = id;
    const chatOn_view = this.chats.find(chat => chat.client[0]._id === id || chat.client[1]._id === id);
    // // console.log(chatOn_view);
    this.changing_chat = true;
    if (chatOn_view) {
      this.dts.getChat(chatOn_view._id, null).then(chat => {
        this._id_chat_on_view = chat.body[0]._id;
        this.chat_on_views = chat.body[0];
        // // console.log(chat);
        this.ngAfterViewInit();
        this.changing_chat = false;
      });
    } else {
      this.dts.getChat(null, id).then(chat => {
        // // console.log('chat created');
        // // console.log('chat', chat);
        this._id_chat_on_view = chat._id_chat;
        this.dts.getChat(chat._id_chat, null).then(chats => {
          this._id_chat_on_view = chats.body[0]._id;
          this.chat_on_views = chats.body[0];
          this.changing_chat = false;
          // // // console.log(chat);
          // this.ngAfterViewInit();
        });
      });
    }
    this.show_chat = this.chats.find(chat => chat._id_user_on_chat === id)
    ? this.chats.find(chat => chat._id_user === this._id_user_on_chat) : null;
  }
  sendMessage(textarea) {
    // // console.log(this._id_chat_on_view);
    let text = textarea.value;
    textarea.value = '';
    // // console.log(this._id_chat_on_view);
    if (text.trim() !== '') {
      text = text.trim();
      this.chat_on_views.messages.push({
        date_send: '',
        text: text,
        _id_sender: this.user._id
      });
      // console.log(this._id_chat_on_view);

      this.ngAfterViewInit();
      // console.log(this._id_chat_on_view);
      this.dts.updateChat(this._id_chat_on_view, text, this.user._id).then(data => {
        // // console.log(data);
        this.dts.getChat(this._id_chat_on_view, null).then(chat => {
          // console.log(chat);
          this._id_chat_on_view = chat.body[0]._id;
          this.chat_on_views = chat.body[0];
          setTimeout(() => {
            this.dts.getAllChat().then(data_chats => this.chats = data_chats.body);
            this.ngAfterViewInit();
          }, 1000);

        });

      });
    }
  }
  messageUser(_id) {
    const messages = this.chats.find(chat => chat.client[0]._id === _id || chat.client[1]._id === _id);
    // // console.log(_id);
    // // console.log(messages);
    if (messages && messages.messages[0] && messages.messages[0].text) {
      // // console.log(messages);
      const mess = messages.messages;
      return mess[mess.length - 1].text;
    } else {
      return this.db.no_message[this.lang];
    }
  }
  findUser(id) {
    return this.users.find(u => u._id === id);
  }
  trackByFn(index, item) {
    return index;
  }

}
