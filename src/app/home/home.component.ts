import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { InscriptionModalComponent } from '../inscription-modal/inscription-modal.component';
import { AlertComponent } from '../alert/alert.component';
import { ResultComponent } from '../result/result.component';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

import { DataServices } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  rForm: FormGroup;
  db: any;
  lang = 'FR';

  list_trano = [];

    constructor(public dialog: MatDialog, public ds: DataServices, private fb: FormBuilder, private route: Router) {
      ds.getLang().subscribe(lang => this.lang = lang);
      ds.getDbLang().subscribe(data => this.db = data);
      if (localStorage.getItem('llew_resu')) {
        this.ds.getUserData(localStorage.getItem('llew_resu')).then(data => {
          this.ds.user.next(data.body[0]);
          this.route.navigate(['/feed']);
        });
      }
      ds.getAllAnnonces()
        .then(data => {
          for (const trano of data) {
            this.list_trano.push({
              type: trano.type_annonce,
              city: trano.address_annonce,
              price: trano.price_annonce,
              pic: trano.pic_annonce[0]
            });
          }
        });
        
          this.rForm = fb.group({
            'price': [null, Validators.required],
            'type': [null, Validators.required]
          });
     }

  ngOnInit() {
    console.log('Wellcoloc initializing...');
  }

  openInscription(): void {
    const dialogRef = this.dialog.open(InscriptionModalComponent, {
      height: 'auto',
      width: 'auto',
      data: { showText: true }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  findAnnonce(donne) {
    // console.log(donne);
    // this.ds.findAnnonce(donne)
    // .then(data => {
    //   console.log(data);
    //   if (data.length === 0) {
    //     this.openAlert({
    //       data: {
    //         type: 'danger',
    //         title: 'Aucun résultat trouvé !',
    //       }
    //     });
    //   } else {
    //     this.dialog.open(ResultComponent, {
    //       minWidth: '500px',
    //       width: 'auto',
    //       height: window.innerHeight - 10 + 'px',
    //       data: data
    //     });
    //   }
    // });
  }
  openAlert(contenu) {
    this.dialog.open(AlertComponent, contenu);
  }
  search(price, type) {
    console.log('price ' + price + ' | type ' + type);
    this.route.navigate(['/search'], {queryParams: {price: price, type: type}});
  }
}
