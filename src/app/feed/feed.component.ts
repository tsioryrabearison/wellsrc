
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { AlertComponent } from '../alert/alert.component';


import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { DataServices } from '../data.service';
import { ModifAnnonceComponent } from '../modif-annonce/modif-annonce.component';
import { ShowPicAnnonceComponent } from '../show-pic-annonce/show-pic-annonce.component';
import { CommentComponent } from '../comment/comment.component';
import { PublicationComponent } from '../publication/publication.component';
import { DialogOuiNonComponent } from '../dialog-oui-non/dialog-oui-non.component';
import * as $ from 'jquery';
// import { CryptoJS } from 'crypto-js';
// test import socket.io
@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss', '../home/home.component.scss']
})
export class FeedComponent implements OnInit {
  list_trano: any;
 pub = [
    {'text': 'votre pub par ici',
     'pic': '../../assets/img/pub2.jpeg'
    },
    {'text': 'votre pub par ici',
     'pic': '../../assets/img/pub.jpeg'
    }
  ];
  utilisateurs: any;
  annonce: any;
  user: any;
  showPage = false;
  users: any;
  annonce_bak: any;
  lang: String = 'FR';
  db: Object;
  constructor(
    public  dialog: MatDialog,
    private route: ActivatedRoute,
    private dts: DataServices,
    private router: Router) {
      dts.getLang().subscribe(lang => this.lang = lang);
      dts.getDbLang().subscribe(data => this.db = data);
      this.dts.user.subscribe(u => this.user = u);
      if (!this.user._id && !localStorage.getItem('llew_resu')) {
        this.router.navigate(['/home']);
      } else {
        if (!this.user._id) {
          console.log('testing the best');
            this.dts.getUserData(localStorage.getItem('llew_resu')).then(data => {
              this.dts.user.next(data.body[0]);
              this.dts.user.subscribe(u => this.user = u);
              this.dts.getAllAnnonces()
              .then(annonces => {
                  this.dts.getAllUsers().then(user => {
                      annonces.body.map(annonce => annonce.user = user.body.find(u => u._id === annonce._id_user));
                      this.users = user.body;
                  });
                  this.annonce = annonces.body;
                // console.log(this.annonce);
                this.showPage = true;
              });
            });
        } else {
          this.dts.user.subscribe(u => this.user = u);
          this.dts.getAllAnnonces()
          .then(annonces => {
              this.dts.getAllUsers().then(user => {
                  annonces.body.map(annonce => annonce.user = user.body.find(u => u._id === annonce._id_user));
                  this.users = user.body;
              });
              this.annonce = annonces.body;
            // console.log(this.annonce);
            this.showPage = true;
          });
        }
      }
    this.reaction = (_id_client, reaction, idPublication) => {
        const value = {
          _id_user: this.user._id,
          _id_client: _id_client,
        };
        // console.log(idPublication);
        // console.log(value);
        // console.log(reaction);
        this.dts.updateReaction(idPublication, reaction, value)
            .then(data => {
              this.dts.getAnnonce(idPublication)
              .then(annonces => {
                  this.annonce.map(annonce => {
                      if (annonce._id === idPublication) {
                        annonce.reaction_annonce = annonces.body[0].reaction_annonce;
                      }
                  });
              });
            });

        //   // console.log(`${reaction} on pub ${idPublication}`);
        //   this.dts.getReaction(reaction, idPublication, this.dts.User.id_user).then(data => {
        //     // console.log(data);
        //     this.dts.getAllAnnonce().then(data2 => {
        //       this.annonce = data2;
        //       // console.log(this.annonce);
        //     });
        //   });
        //   if (reaction === 'like') {
        //     this.likeFunction(idPublication);
        //   } else if ( reaction === 'comment') {
        //     // this.commentFunction(idPublication);
        //   } else if ( reaction === 'follow') {
        //     this.followFunction(idPublication);
        //   } else if ( reaction === 'rate') {
        //     this.rateFunction(idPublication);
        //   }
        // };

      };
  // ) {
    // --------------------------------REDIRECTION
  //   // console.log(this.dts.User);
  //   // --------------------------------DATABASE LOADING...
  //   dts.getAllAnnonce().then(data => {
  //     // console.log('All annonce loaded');
  //     this.annonce = data;
  //   });
  //   dts.getUsers().then(data =>  {
  //     // console.log('All users checked');
  //     this.utilisateurs = data;
  //   });
  //   // User to dts
  //   // dts.getUserInfo(2).then(data => {
  //   //   // console.log('User info loaded');
  //   //   this.User = data[0];
  //   //   // console.log(this.dts.User.id_user);
  //   // });
  //   // ---------------------------------reaction function
  //   this.reaction = (reaction, idPublication) => {
  //     // console.log(`${reaction} on pub ${idPublication}`);
  //     dts.getReaction(reaction, idPublication, this.dts.User.id_user).then(data => {
  //       // console.log(data);
  //       this.dts.getAllAnnonce().then(data2 => {
  //         this.annonce = data2;
  //         // console.log(this.annonce);
  //       });
  //     });
  //     // if (reaction === 'like') {
  //     //   // moins 1 car indice du tableau liste_trano commence par 0 or id commence par 1
  //     //   this.likeFunction(idPublication);
  //     // } else if ( reaction === 'comment') {
  //     //   // this.commentFunction(idPublication);
  //     // } else if ( reaction === 'follow') {
  //     //   this.followFunction(idPublication);
  //     // } else if ( reaction === 'rate') {
  //     //   this.rateFunction(idPublication);
  //     // }
  //   };
  //   // comment fuction
  //   this.addComment = (forResize, texte, commentaireContainer, idPublication) => {
  //     if (texte.value !== '') {
  //       dts.getComment('comment', idPublication, this.dts.User.id_user, texte.value).then(data => {
  //         texte.value = '';
  //         this.resizeTextarea(forResize[0], forResize[1], forResize[2], forResize[3]);

  //         this.dts.getAllAnnonce().then(data2 => {
  //           this.annonce = data2;
  //           // console.log(this.annonce);
  //           setTimeout(() => {
  //             commentaireContainer.scrollTo({top: commentaireContainer.scrollHeight});
  //           }, 10);
  //         });
  //       });
  //     }
  //   };
  // }
    }
  ngOnInit() {
    this.loadingAnimation();
  }
  isLike(reaction) {
    return reaction.find(arr => arr._id_user === this.user._id) ? false : true;
  }
  openDialogPublication() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.height = window.innerHeight - 5 + 'px';
    dialogConfig.data = {
      callback: () => {
        this.dts.getAllAnnonces()
        .then(annonces => {
            this.dts.getAllUsers().then(user => {
                annonces.body.map(annonce => annonce.user = user.body.find(u => u._id === annonce._id_user));
                this.users = user.body;
            });
            this.annonce = annonces.body;
            this.dialog.open(AlertComponent, {
              data: {
                title: this.lang === 'FR' ? 'Enregistrée' : 'Saved',
              }
            });
          });

      }
    };

    this.dialog.open(PublicationComponent, dialogConfig);
  }
  loadingAnimation() {
    const loadingDot = [ '.', '..', '...'];
    let index = 0;
    const loading = setInterval(() => {
      if (!this.showPage || !this.user || !this.annonce || !this.users) {
        const loadingComponent = document.getElementById('loading');
        if (loadingComponent) {
          loadingComponent.innerText = loadingDot[index];
          index = index >= 2 ? 0 : index + 1;
        }
      }
    }, 800);
    setTimeout(() => {
      this.showPage = true;
      if (this.annonce && this.utilisateurs && this.user && this.showPage) {
        clearInterval(loading);
      }
    }, 2500);
  }

  // resizeTextarea(textarea, textareaContainer, clone, idPublication) {
  //   let commentaireHeight;
  //   let textareaScrollHeight;
  //   let textareaHeight;

  //   if (document.getElementById(`commentaires${idPublication}`).style.height !== '') {
  //     commentaireHeight = parseInt(document.getElementById(`commentaires${idPublication}`).style.height, 10);
  //   } else {
  //     commentaireHeight = parseInt(getComputedStyle(document.getElementById(`commentaires${idPublication}`)).height, 10);
  //   }

  //   textareaContainer.style.height = '60px';
  //   clone.style.height = '60px';
  //   textareaScrollHeight = textarea.scrollHeight;
  //   textareaHeight = getComputedStyle(textarea, null).height;
  //   textareaContainer.style.height = (textareaScrollHeight + 20) + 'px';
  //   clone.style.height = (textareaScrollHeight + 20) + 'px';
  //   if (parseInt(textareaContainer.style.height, 10) + commentaireHeight > 400 &&  parseInt(textareaContainer.style.height, 10) < 224) {
  //     document.getElementById(`commentaires${idPublication}`).style.height = 400 - parseInt(textareaContainer.style.height, 10) + 'px';
  //   }
  // }
  reaction(_id_client, reaction, idPublication) {
      alert('Une erreur est survenue, nous vous prions de reloader.');
  }
  // rateFunction(idPublication) {

  // }

  // followFunction(idPublication) {
  //   if (this.annonce[idPublication].reaction_annonce.follow.indexOf(this.dts.User.id_user) === -1) {
  //     // ajout de l'id de l'utilisateur dans le tableau like de la publication correspondante
  //     this.annonce[idPublication].reaction_annonce.follow.push(this.dts.User.id_user);

  //     // for trackby and reload
  //     const trackTrap = this.annonce;
  //     this.annonce = trackTrap;
  //   } else {
  //     // on retire l'id de l'utilasateur
  //     const maximumValueOfI = this.annonce[idPublication].reaction_annonce.follow.length;
  //     const newLikeTable = [];
  //     for (let i = 0; i < maximumValueOfI; i++) {
  //       if (i !== this.list_trano[idPublication].more.followers.indexOf(this.dts.User.id_user)) {
  //           newLikeTable.push(this.list_trano[idPublication].more.followers[i]);
  //       }
  //     }
  //     this.list_trano[idPublication].more.followers = newLikeTable;
  //     // for trackby and reload
  //     const trackTrap = this.list_trano;
  //     this.list_trano = trackTrap;
  //   }
  // }
  // showComment(idPublication) {
  //   const commentPublication = document.getElementById(`comment${idPublication}`);
  //   const commentaireContainer = document.getElementById(`commentaires${idPublication}`);
  //   const indexHidden = commentPublication.className.indexOf('hidden');
  //   // console.log('showComment' + commentPublication);
  //   if (indexHidden !== -1) {
  //     const part1Class = commentPublication.className.substring(0, indexHidden);
  //     const part2Class = commentPublication.className.substring(indexHidden + 7, commentPublication.className.length);
  //     commentPublication.className = part1Class + part2Class;
  //     setTimeout(() => {
  //       commentaireContainer.scrollTo({top: commentaireContainer.scrollHeight});
  //     }, 1);
  //   } else {
  //     commentPublication.className += ' hidden';
  //   }
  // }
  // likeFunction(idPublication) {
  //   if (this.list_trano[idPublication].more.like.indexOf(this.dts.User.id_user) === -1) {
  //       // ajout de l'id de l'utilisateur dans le tableau like de la publication correspondante
  //       this.list_trano[idPublication].more.like.push(this.dts.User.id_user);

  //       // for trackby and reload
  //       const trackTrap = this.list_trano;
  //       this.list_trano = trackTrap;
  //     } else {
  //       // on retire l'id de l'utilasateur
  //       const maximumValueOfI = this.list_trano[idPublication].more.like.length;
  //       const newLikeTable = [];
  //       for (let i = 0; i < maximumValueOfI; i++) {
  //         if (i !== this.list_trano[idPublication].more.like.indexOf(this.dts.User.id_user)) {
  //             newLikeTable.push(this.list_trano[idPublication].more.like[i]);
  //         }
  //       }
  //       this.list_trano[idPublication].more.like = newLikeTable;
  //       // for trackby and reload
  //       const trackTrap = this.list_trano;
  //       this.list_trano = trackTrap;
  //     }
  // }
  // addComment(forResize, texte, commentaireContainer, idPublication) {
  //   alert('Erreur! Veuillez reloader');
  // }
  // findUser(idUser) {
  //     return this.utilisateurs.find(user => user.id_user === idUser);
  // }
  openModifAnnonce(id_annonce) {
    this.dialog.open(ModifAnnonceComponent, {
      data: {
        id_annonce: id_annonce,
        callback: () => {
          this.dts.getAllAnnonces()
          .then(annonces => {
              this.dts.getAllUsers().then(user => {
                  annonces.body.map(annonce => annonce.user = user.body.find(u => u._id === annonce._id_user));
                  this.annonce = annonces.body;
              });
            // console.log(this.annonce);
            this.showPage = true;
          });
        }
      }
    });
  }
  showPic(annonce) {
    this.dialog.open(ShowPicAnnonceComponent, {
      maxWidth: '600px',
      maxHeight: window.innerHeight - 15 + 'px',
        data: {
          annonce: annonce
        }
    });
  }
  goToChat(id_user) {
    this.router.navigate(['/chat'], {queryParams: {id_client: id_user}});
  }
  goToUserProfil(id_user) {
    this.router.navigate(['/user-profil'], {queryParams: {id: id_user}});
  }
  // calculeAge(date_birth) {
  //     let data_birthTable = date_birth.split('-');
  //     const data_birthTableInt = [];
  //     for (data_birthTable of data_birthTable) {
  //       data_birthTableInt.push(parseInt(data_birthTable, 10));
  //     }
  //     const date = new Date();
  //     const mois = date.getMonth();
  //     const anne = date.getFullYear();
  //     const jour = date.getDay();
  //     let age = anne - data_birthTableInt[2] - 1;
  //     if (mois >= data_birthTableInt[1]) {
  //       if (mois === data_birthTableInt[1]) {
  //         if (jour >= data_birthTableInt[0]) {
  //           age++;
  //         }
  //         } else {
  //             age++;
  //         }
  //     }
  //      return age;
  // }
  trackByFn(index, item) {
    return index;
  }

  openComment(annonce) {
    this.dialog.open(CommentComponent, {
      data: {
        annonce: annonce,
        users: this.users,
        callback: () => {
          this.dts.getAllAnnonces()
          .then(annonces => {
              this.showPage = false;
              this.dts.getAllUsers().then(user => {
                annonces.body.map(_annonce => _annonce.user = user.body.find(u => u._id === _annonce._id_user));
                this.users = user.body;
            });
            this.annonce = annonces.body;
          // console.log(this.annonce);
          this.showPage = true;
          });
        }
      }
    });
  }

  filterAnnonce(prix, type) {
    console.log(`prix : ${prix} | type : ${type}`);
  }
  toggle_el(id) {

  }
  delete_publication(_id) {
    const self = this;
    const del = this.lang === 'FR' ? 'Supprimer ?' : 'Delete ?';
    this.dialog.open(DialogOuiNonComponent, {
      data: {
        text: {
          title: del,
        },
        ifAccepted: () => {
          this.dts.deleteAnnonce(_id).then(d => {
            this.dts.getAllAnnonces()
              .then(annonces => {
                this.dts.getAllUsers().then(user => {
                    annonces.body.map(annonce => annonce.user = user.body.find(u => u._id === annonce._id_user));
                    this.annonce = annonces.body;
                    this.dialog.open(AlertComponent, {
                      data: {
                        title: self.lang === 'fr' ? 'Effectué' : 'Done'
                      }
                    });
                });
               
            });

          });
        }
      }
    });
  }

}
