import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  constructor(private dialogref: MatDialogRef<LoadingComponent>) { }

  ngOnInit() {
  }

  close() {
    this.dialogref.close();
  }
}
