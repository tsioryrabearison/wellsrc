import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataServices } from '../data.service';
import { UserService } from '../user.service';

import { ModifAnnonceComponent } from '../modif-annonce/modif-annonce.component';
import { ModifProfileComponent } from '../modif-profile/modif-profile.component';
import { DialogOuiNonComponent } from '../dialog-oui-non/dialog-oui-non.component';
import { AlertComponent } from '../alert/alert.component';

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.component.html',
  styleUrls: ['./user-profil.component.scss', '../feed/feed.component.scss']
})
export class UserProfilComponent implements OnInit {
  users: any;
  user: any;
  annonce: any;
  showPage = false;
  timeout = false;
  _id_user: any;
  showChoise = false;
  pic = {
    preview: 'assets/img/userH.png',
    blob: null
  };
  rForm: FormGroup;
  lang: any;
  db: any;
  constructor(
    private dts: DataServices,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {

      this.rForm = fb.group({
        'firstName': [Validators.compose([Validators.pattern(/^[\w \-àäâîïûüùéèêëôöç]+$/)])],
        'lastName': [Validators.compose([Validators.required, Validators.pattern(/^[\w \-àäâîïûüùéèêëôöç]+$/)])],
        'address_user': [Validators.compose([Validators.required, ])],
        'phone': [Validators.compose([Validators.required, Validators.pattern(/^[\- 0-9+]+$/)])],
        'mail': [Validators.compose([Validators.required, Validators.email])],
      });

      this.dts.getLang().subscribe(l => this.lang = l);
      this.dts.getDbLang().subscribe(db => this.db = db);
      if (this.dts.user) {
        this.dts.user.subscribe(u => {
          this.user = u;
          if (u._id) {
            this.pic.preview = u.pic_prof[0];
          }

        });

    this.rForm.value.firstName = this.user.firstName;
    this.rForm.value.lastName = this.user.lastName;
    this.rForm.value.address_user = this.user.address_user;
    this.rForm.value.phone = this.user.phone;
    this.rForm.value.mail  = this.user.mail;

        this._id_user = this.user._id;
        // dts.getUserInfo(this.id_user).then(res => {
        //   this.User = res[0];
        //   this.pic.preview = this.User.pic_prof[0];
        // });
        this.dts.getUserData(this._id_user).then(data => {
            this.user = data.body[0];
            this.dts.getUserAnnonces().then(annonce => {
              this.annonce = annonce.body;
            });
        });
      } else {
        const link = ['/home'];
        this.router.navigate(link);
      }
    // this.pic.preview = this.dts.User.pic_prof[0];
    // this.route.queryParams.subscribe(params => {
    //   if (params.id) {
    //     this.id_user = parseInt(params.id, 10);
    //     dts.getUserInfo(this.id_user).then(res => {
    //       this.User = res[0];
    //       this.pic.preview = this.User.pic_prof[0];
    //     });
    //   } else {
    //     const link = this.dts.User ? ['/feed'] : ['/home'];
    //     this.router.navigate(link);
    //   }
    // });
    // dts.getUsers().then(data =>  {
    //   // console.log(data);
    //   this.users = data;
    // });
    // setTimeout(() => {
    //   this.timeout = true;
    // }, 9000);
    // const loadUser = setInterval(() => {
    //   if (this.dts.User) {
    //     this.dts.getAnnonceUser(this.id_user).then(data => {
    //       console.log(data);
    //       this.annonce = data;
    //     clearInterval(loadUser);
    //   });
    // }
    // }, 10);


    // this.reaction = (reaction, idPublication) => {
    //   console.log(`${reaction} on pub ${idPublication}`);
    //   dts.getReaction(reaction, idPublication, this.dts.User.id_user).then(data => {
    //     console.log(data);
    //     this.dts.getAllAnnonce().then(data2 => {
    //       this.annonce = data2;
    //       console.log(this.annonce);
    //     });
    //   });
    // };
    //   this.addComment = (forResize, texte, commentaireContainer, idPublication) => {
    //     if (texte.value !== '') {
    //       dts.getComment('comment', idPublication, this.dts.User.id_user, texte.value).then(data => {
    //         texte.value = '';
    //         this.resizeTextarea(forResize[0], forResize[1], forResize[2], forResize[3]);
    //         this.dts.getAllAnnonce().then(data2 => {
    //           this.annonce = data2;
    //           console.log(this.annonce);
    //           setTimeout(() => {
    //             commentaireContainer.scrollTo({top: commentaireContainer.scrollHeight});
    //           }, 10);
    //         });
    //       });
    //     }
    //   };
    }

  ngOnInit() {
  }
  openModifProfil(aModifier) {
    this.dialog.open(ModifProfileComponent, {
      data: {
        aModifier: aModifier,
        callback: () => {
          this.dts.getUserData(this.user._id).then(user => {
            if (this.dts.user) {
                this.dts.user.next(user.body[0]);
                this.dts.user.subscribe(u => this.user = u);
              }
          });
        }
      }
    });
  }
  showModifChoice() {
    this.showChoise = this.showChoise ? false : true;
  }
  // resizeTextarea(textarea, textareaContainer, clone, idPublication) {
  //   let commentaireHeight;
  //   let textareaScrollHeight;
  //   let textareaHeight;

  //   if (document.getElementById(`commentaires${idPublication}`).style.height !== '') {
  //     commentaireHeight = parseInt(document.getElementById(`commentaires${idPublication}`).style.height, 10);
  //   } else {
  //     commentaireHeight = parseInt(getComputedStyle(document.getElementById(`commentaires${idPublication}`)).height, 10);
  //   }

  //   textareaContainer.style.height = '60px';
  //   clone.style.height = '60px';
  //   textareaScrollHeight = textarea.scrollHeight;
  //   textareaHeight = getComputedStyle(textarea, null).height;
  //   textareaContainer.style.height = (textareaScrollHeight + 20) + 'px';
  //   clone.style.height = (textareaScrollHeight + 20) + 'px';
  //   if (parseInt(textareaContainer.style.height, 10) + commentaireHeight > 400 &&  parseInt(textareaContainer.style.height, 10) < 224) {
  //     document.getElementById(`commentaires${idPublication}`).style.height = 400 - parseInt(textareaContainer.style.height, 10) + 'px';
  //   }
  // }
  // reaction(reaction, idPublication) {
  //     alert('Une erreur est survenue, nous vous prions de reloader.');
  // }
  // rateFunction(idPublication) {

  // }

  // showComment(idPublication) {
  //   const commentPublication = document.getElementById(`comment${idPublication}`);
  //   const commentaireContainer = document.getElementById(`commentaires${idPublication}`);
  //   const indexHidden = commentPublication.className.indexOf('hidden');
  //   console.log('showComment' + commentPublication);
  //   if (indexHidden !== -1) {
  //     const part1Class = commentPublication.className.substring(0, indexHidden);
  //     const part2Class = commentPublication.className.substring(indexHidden + 7, commentPublication.className.length);
  //     commentPublication.className = part1Class + part2Class;
  //     setTimeout(() => {
  //       commentaireContainer.scrollTo({top: commentaireContainer.scrollHeight});
  //     }, 1);
  //   } else {
  //       commentPublication.className += ' hidden';
  //   }
  // }
  // addComment(forResize, texte, commentaireContainer, idPublication) {
  //   alert('Erreur! Veuillez reloader');
  // }
  // findUser(idUser) {
  //     return this.users.find(user => user.id_user === idUser);
  // }
  // openModifAnnonce(id_annonce) {
  //   this.dialog.open(ModifAnnonceComponent, {
  //     data: {
  //       id_annonce: id_annonce,
  //       callback: () => {
  //         this.dts.getAnnonceUser(this.id_user).then(dat2 => {
  //           this.annonce = dat2;
  //         });
  //       }
  //     }
  //   });
  // }
  // goToChat(id_user) {
  //   this.router.navigate(['/chat'], {queryParams: {id_client: id_user}});
  // }
  // calculeAge(date_birth) {
  //     let data_birthTable = date_birth.split('-');
  //     const data_birthTableInt = [];
  //     for (data_birthTable of data_birthTable) {
  //       data_birthTableInt.push(parseInt(data_birthTable, 10));
  //     }
  //     const date = new Date();
  //     const mois = date.getMonth();
  //     const anne = date.getFullYear();
  //     const jour = date.getDay();
  //     let age = anne - data_birthTableInt[2] - 1;
  //     if (mois >= data_birthTableInt[1]) {
  //       if (mois === data_birthTableInt[1]) {
  //         if (jour >= data_birthTableInt[0]) {
  //           age++;
  //         }
  //         } else {
  //             age++;
  //         }
  //     }
  //      return age;
  // }

  fileChange (event) {
    console.log('event');
    const reader = new FileReader();
    const file = event.target.files;
    if (file[0] && file[0].type !== 'image/png') {
      console.log('here');
      reader.readAsDataURL(file[0]);
      reader.onloadend = () => {
        this.pic.preview = '' + reader.result;
        this.pic.blob = reader.result.toString().split('base64,')[1].trim();
        const fileName = new Date().valueOf();
        console.log(this.pic.blob);
        if (this.pic.blob) {
          this.dts.getUrlFileUploaded(
            'pic_profile/' + this.user._id,
            fileName,
            this.pic.blob
          ).then(res => {
            console.log(res);
            if (res.success) {
              this.dts.updateUser({
                  pic_prof: [res.imgUrl]
              }).then(data2 => {
                this.dts.getUserData(this.user._id).then(data => {
                  this.dts.user.next(data.body[0]);
                });
                this.dialog.open(AlertComponent, {
                  data: {
                    title: this.lang === 'FR' ? 'Modification terminée.' : 'Done.'
                  }
                });
              });
            }
          });
        }
    };
  }



    //     const file = event.target.files;
    //     if (file[0]) {
    //       if (file[0].type !== 'image/jpeg' && file[0].type !== 'image/png') {
    //       } else if (file[0].size > 25000000) {
    //         // opt.message = 'L\'image doit être inférieure à 25MB';
    //         // toast.present();
    //       } else {

    //         reader.readAsDataURL(file[0]);
    //         reader.onloadend = () => {
    //           this.dialog.open(DialogOuiNonComponent, {
    //             data: {
    //               text: {
    //                 title: 'Confirmer modification'
    //               },
    //               ifAccepted: () => {
    //                 this.pic.preview = '' + reader.result;
    //                 this.pic.blob = reader.result.split('base64,')[1].trim();
    //                 this.dts.updateUser({
    //                     pic_prof: [this.pic.blob]
    //                 }).then(data2 => {
    //                   console.log(data2);
    //                   this.dialog.open(AlertComponent, {
    //                     data: {
    //                       title: 'Modification terminée.'
    //                     }
    //                   });
    //                 });
    //                 console.log(this.pic);
    //               },
    //               ifRefused: () => {
    //                 console.log('dialog closed');
    //               }
    //             }
    //           });
    //         };
    //       }
    //     }
    // }
  }
  modif(value, elem) {
    const donne: Object = {
      firstName: this.rForm.value.firstName,
      lastName:  this.rForm.value.lastName,
      address_user:  this.rForm.value.address_user,
      phone:  this.rForm.value.phone,
      mail:  this.rForm.value.mail
    };
    this.dts.updateUser(donne).then(data2 => {
      document.getElementById('modif_profile').style.transform = 'scale(0)';
      document.getElementById('modif_profile').style.opacity = '0';
      this.dialog.open(AlertComponent, {
        data: {
          title: this.lang === 'FR' ? 'Modifications terminées.' : 'Done'
        }
      });
    });
  }
  trackByFn(index) {
    return index;
  }

}
