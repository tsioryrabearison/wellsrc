import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-faq',
  templateUrl: './page-faq.component.html',
  styleUrls: ['./page-faq.component.scss']
})
export class PageFaqComponent implements OnInit {

  faq = [
    {
      title: 'A quoi sert le site et l\'application Wellcoloc. Com ?',
      description: `
          Wellcoloc.com est un service de mise en relations entre les utilisateurs,
          qui proposent une colocation, une location, et ceux qui recherchent,
          en matchant en plus leurs intérêts communs.`
    },
    {
      title: `Comment puis-je m'inscrire ?`,
      description: `
          L'inscription peut se faire, de plusieurs manières :
          soit en créant un compte par mail, téléphone, où par le biais des réseaux sociaux
          existants ( facebook, Twitter, Linkedin..) la liste n'étant pas exhaustive.
          Un téléphone valide est requis pour tout utilisateur.`
    },
    {
      title: `L'utilisation du service est-il payant ?`,
      description: `
          Après inscription validée de l'utilisateur, la recherche, ainsi que l'usage de toutes
          les fonctionnalités du site et de l'application sont totalement gratuit,
          durant les jours qui suivent celle-ci, la direction se réserve cependant  le droit de
          le rendre payant, sans préavis , après une période avantageuse offerte au bénéfice de
          l'utilisateur.`
    },
    {
      title: `Le site est- il sécurisé ainsi que les transactions ?`,
      description: `
          Nous nous efforçons d'ores et déjà à rendre le service, le plus sécuriser possible,
          en usant des dernières technologies en matières de système de sécurité web.
          Les données des moyens de paiement ne sont pas stockées automatiquement sur notre site,
          sauf demande express de l'utilisateur pour ses prochaines transactions.`
    },
    {
      title: `Combien coûte l'abonnement wellcoloc ?`,
      description: `
          Pour connaître le tarif des abonnements du service, rendez vous au rubrique,
          tarif et abonnements en bas de page du site.`
    },
    {
      title: `Pourrais-je me désinscrire lorsque j'ai trouvé la colocation ou l'appartement idéal ?`,
      description: `
          Lorsque l'utilisateur ne souhaite plus faire usage de son compte,
          il pourra alors le désactiver, et le réactiver de nouveau avec es même identifiants
          si besoin.`
    },
    {
      title: `Dans quelles régions du monde puis- je utiliser le service pour mes recherches ?`,
      description: `
          Le service wellcoloc est mondial, présent sur les 5 continents, principalement en Europe.`
    },
    {
      title: `Que faire en cas de dysfonctionnement sur l'application ou le site ?`,
      description: `
          L'utilisateur pourra alors envoyer un message au service maintenance du site,
          l'adresse mail figure en bas de page, nous vous répondrons dans un délai de 48 heures
          maximum, jours ouvrables.`
    },
    {
      title: `Rôles du service wellcoloc`,
      description: `
          Le service met en relation simplement les utilisateurs,
          personnes majeures à la date d'inscription, mais ne peut-être en aucun cas
          tenu responsable, des malveillances causées par une personne physique malintentionnée
          en dehors du cadre de la mise en relation.`
    },
    {
      title: `La modification de mon annonce est-elle payante ?`,
      description: `
          La modification de l'annonce est gratuite l'heure suivant la validation de celle-ci,
          et ne coûtera que 0,99 centimes d'euros au delà de ce délai.`
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
