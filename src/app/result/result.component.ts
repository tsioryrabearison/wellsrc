import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { AlertComponent } from '../alert/alert.component';


import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { DataServices } from '../data.service';
import { ModifAnnonceComponent } from '../modif-annonce/modif-annonce.component';
import { ShowPicAnnonceComponent } from '../show-pic-annonce/show-pic-annonce.component';
import { CommentComponent } from '../comment/comment.component';
import { PublicationComponent } from '../publication/publication.component';
import { DialogOuiNonComponent } from '../dialog-oui-non/dialog-oui-non.component';
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss', '../feed/feed.component.scss']
})
export class ResultComponent implements OnInit {

    list_trano: any;
   pub = [
      {'text': 'votre pub par ici',
       'pic': '../../assets/img/pub2.jpeg'
      },
      {'text': 'votre pub par ici',
       'pic': '../../assets/img/pub.jpeg'
      }
    ];
    resultAnnonces: any;
    utilisateurs: any;
    annonce: any;
    user: any;
    showPage = false;
    users: any = {_id: null};
    annonce_bak: any;
    lang: String = 'FR';
    db: Object;
    constructor(
      public  dialog: MatDialog,
      private route: ActivatedRoute,
      private dts: DataServices,
      private router: Router) {
        dts.getLang().subscribe(lang => this.lang = lang);
        dts.getDbLang().subscribe(data => this.db = data);
        const self = this;
        this.dts.getAllAnnonces().then(annonces => {
            self.dts.getAllUsers().then(user => {
                  annonces.body.map(annonce => annonce.user = user.body.find(u => u._id === annonce._id_user));
                  self.users = user.body;

              self.annonce = annonces.body;
              self.route.queryParams.subscribe((params: any) =>  {
                self.resultAnnonces = this.annonce.filter(annonce => {
                    return ((parseInt(annonce.price_annonce, 10) <= (parseInt(params.price, 10)) + parseInt(params.price, 10) * 0.5)) &&
                    ((parseInt(annonce.price_annonce, 10) >= (parseInt(params.price, 10)) - parseInt(params.price, 10) * 0.5)) &&
                    annonce.type_annonce === params.type;
                });
                // console.log(this.annonce);
                self.showPage = true;
                if (!self.resultAnnonces || !self.resultAnnonces[0]) {
                  console.log('no result');
                  this.dialog.open(AlertComponent, {
                    data: {
                      title: this.lang === 'FR' ? 'Aucun resultat' : 'No result',
                      callback: () => {
                        this.router.navigate(['/home']);
                      }
                    }
                  });
                }
          });
        });
      });

      }
    ngOnInit() {
      this.loadingAnimation();
    }
    isLike(reaction) {
      return reaction.find(arr => arr._id_user === this.user._id) ? false : true;
    }
    openDialogPublication() {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.height = window.innerHeight - 5 + 'px';
      dialogConfig.data = {
        callback: () => {
          this.dts.getAllAnnonces()
          .then(annonces => {
              this.dts.getAllUsers().then(user => {
                  annonces.body.map(annonce => annonce.user = user.body.find(u => u._id === annonce._id_user));
                  this.users = user.body;
              });
              this.annonce = annonces.body;
              this.dialog.open(AlertComponent, {
                data: {
                  title: this.lang === 'FR' ? 'Enregistrée' : 'Saved',
                }
              });
            });

        }
      };

      this.dialog.open(PublicationComponent, dialogConfig);
    }
    loadingAnimation() {
      const loadingDot = [ '.', '..', '...'];
      let index = 0;
      const loading = setInterval(() => {
        if (!this.showPage || !this.user || !this.annonce || !this.users) {
          const loadingComponent = document.getElementById('loading');
          if (loadingComponent) {
            loadingComponent.innerText = loadingDot[index];
            index = index >= 2 ? 0 : index + 1;
          }
        }
      }, 800);
      setTimeout(() => {
        this.showPage = true;
        if (this.annonce && this.utilisateurs && this.user && this.showPage) {
          clearInterval(loading);
        }
      }, 2500);
    }
    reaction(_id_client, reaction, idPublication) {
        alert('Une erreur est survenue, nous vous prions de reloader.');
    }

    openModifAnnonce(id_annonce) {
      this.dialog.open(ModifAnnonceComponent, {
        data: {
          id_annonce: id_annonce,
          callback: () => {
            this.dts.getAllAnnonces()
            .then(annonces => {
                this.dts.getAllUsers().then(user => {
                    annonces.body.map(annonce => annonce.user = user.body.find(u => u._id === annonce._id_user));
                    this.annonce = annonces.body;
                });
              // console.log(this.annonce);
              this.showPage = true;
            });
          }
        }
      });
    }
    showPic(annonce) {
      this.dialog.open(ShowPicAnnonceComponent, {
        maxWidth: '600px',
        maxHeight: window.innerHeight - 15 + 'px',
          data: {
            annonce: annonce
          }
      });
    }
    goToChat(id_user) {
      this.router.navigate(['/chat'], {queryParams: {id_client: id_user}});
    }
    goToUserProfil(id_user) {
      this.router.navigate(['/user-profil'], {queryParams: {id: id_user}});
    }

    trackByFn(index, item) {
      return index;
    }

    openComment(annonce) {
      this.dialog.open(CommentComponent, {
        data: {
          annonce: annonce,
          users: this.users,
          callback: () => {
            this.dts.getAllAnnonces()
            .then(annonces => {
                this.showPage = false;
                this.dts.getAllUsers().then(user => {
                  annonces.body.map(_annonce => _annonce.user = user.body.find(u => u._id === _annonce._id_user));
                  this.users = user.body;
              });
              this.annonce = annonces.body;
            // console.log(this.annonce);
            this.showPage = true;
            });
          }
        }
      });
    }

    filterAnnonce(prix, type) {
      console.log(`prix : ${prix} | type : ${type}`);
    }
    delete_publication(_id) {
      const self = this;
      const del = this.lang === 'FR' ? 'Supprimer ?' : 'Delete ?';
      this.dialog.open(DialogOuiNonComponent, {
        data: {
          text: {
            title: del,
          },
          ifAccepted: () => {
            this.dts.deleteAnnonce(_id).then(d => {
              this.dialog.open(AlertComponent, {
                data: {
                  title: self.lang === 'fr' ? 'Effectué' : 'Done'
                }
              });
            });
          }
        }
      });
    }

  }

